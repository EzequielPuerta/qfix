import unittest
import quickfix as fix
from tests.utils import bymaApplication
from qfix.message.types import *
from qfix.message.generic import buildMessageHeader

class TestMessageTypes(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestMessageTypes, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	#---------------------------------------------------------------------
	# Session and Administrative Messages
	#---------------------------------------------------------------------

	def test_isLogon(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertTrue(isLogon(message))

	def test_isHeartbeat(self):
		message = buildMessageHeader(self.application, fix.MsgType_Heartbeat)
		self.assertTrue(isHeartbeat(message))

	def test_isTestRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_TestRequest)
		self.assertTrue(isTestRequest(message))

	def test_isResendRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_ResendRequest)
		self.assertTrue(isResendRequest(message))

	def test_isReject(self):
		message = buildMessageHeader(self.application, fix.MsgType_Reject)
		self.assertTrue(isReject(message))

	def test_isSequenceReset(self):
		message = buildMessageHeader(self.application, fix.MsgType_SequenceReset)
		self.assertTrue(isSequenceReset(message))

	def test_isLogout(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logout)
		self.assertTrue(isLogout(message))

	def test_isIndicationOfInterest(self):
		message = buildMessageHeader(self.application, fix.MsgType_IOI)
		self.assertTrue(isIndicationOfInterest(message))

	def test_isUserRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_UserRequest)
		self.assertTrue(isUserRequest(message))

	def test_isUserResponse(self):
		message = buildMessageHeader(self.application, fix.MsgType_UserResponse)
		self.assertTrue(isUserResponse(message))

	#---------------------------------------------------------------------
	# Order Handling Messages
	#---------------------------------------------------------------------

	def test_isNewOrderSingle(self):
		message = buildMessageHeader(self.application, fix.MsgType_NewOrderSingle)
		self.assertTrue(isNewOrderSingle(message))

	def test_isOrderCancelRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderCancelRequest)
		self.assertTrue(isOrderCancelRequest(message))

	def test_isOrderMassCancelRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderMassCancelRequest)
		self.assertTrue(isOrderMassCancelRequest(message))

	def test_isOrderMassCancelReport(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderMassCancelReport)
		self.assertTrue(isOrderMassCancelReport(message))

	def test_isOrderCancelReplaceRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderCancelReplaceRequest)
		self.assertTrue(isOrderCancelReplaceRequest(message))

	def test_isOrderCancelReject(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderCancelReject)
		self.assertTrue(isOrderCancelReject(message))

	def test_isExecutionReport(self):
		message = buildMessageHeader(self.application, fix.MsgType_ExecutionReport)
		self.assertTrue(isExecutionReport(message))

	def test_isOrderStatusRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderStatusRequest)
		self.assertTrue(isOrderStatusRequest(message))

	def test_isOrderMassStatusRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_OrderMassStatusRequest)
		self.assertTrue(isOrderMassStatusRequest(message))

	def test_isTradeCaptureReportRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_TradeCaptureReportRequest)
		self.assertTrue(isTradeCaptureReportRequest(message))

	def test_isTradeCaptureReport(self):
		message = buildMessageHeader(self.application, fix.MsgType_TradeCaptureReport)
		self.assertTrue(isTradeCaptureReport(message))

	def test_isAllocationInstruction(self):
		message = buildMessageHeader(self.application, fix.MsgType_AllocationInstruction)
		self.assertTrue(isAllocationInstruction(message))

	def test_isAllocationReport(self):
		message = buildMessageHeader(self.application, fix.MsgType_AllocationReport)
		self.assertTrue(isAllocationReport(message))

	#---------------------------------------------------------------------
	# Market Data Messages
	#---------------------------------------------------------------------

	# Pre-trade Market Information
	def test_isMarketDataRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_MarketDataRequest)
		self.assertTrue(isMarketDataRequest(message))

	def test_isMarketDataSnapshotFullRefresh(self):
		message = buildMessageHeader(self.application, fix.MsgType_MarketDataSnapshotFullRefresh)
		self.assertTrue(isMarketDataSnapshotFullRefresh(message))

	def test_isMarketDataIncrementalRefresh(self):
		message = buildMessageHeader(self.application, fix.MsgType_MarketDataIncrementalRefresh)
		self.assertTrue(isMarketDataIncrementalRefresh(message))

	# Instrument Reference Data
	def test_isSecurityListRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_SecurityListRequest)
		self.assertTrue(isSecurityListRequest(message))

	def test_isSecurityList(self):
		message = buildMessageHeader(self.application, fix.MsgType_SecurityList)
		self.assertTrue(isSecurityList(message))

	def test_isTradingSessionStatusRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_TradingSessionStatusRequest)
		self.assertTrue(isTradingSessionStatusRequest(message))

	def test_isTradingSessionStatus(self):
		message = buildMessageHeader(self.application, fix.MsgType_TradingSessionStatus)
		self.assertTrue(isTradingSessionStatus(message))

	def test_isSecurityDefinitionRequest(self):
		message = buildMessageHeader(self.application, fix.MsgType_SecurityDefinitionRequest)
		self.assertTrue(isSecurityDefinitionRequest(message))

	def test_isSecurityDefinition(self):
		message = buildMessageHeader(self.application, fix.MsgType_SecurityDefinition)
		self.assertTrue(isSecurityDefinition(message))

	def test_isNews(self):
		message = buildMessageHeader(self.application, fix.MsgType_News)
		self.assertTrue(isNews(message))

	# Statistic Data
	def test_isMarketDataStatisticsRequest(self):
		message = buildMessageHeader(self.application, "DO")
		self.assertTrue(isMarketDataStatisticsRequest(message))

	def test_isMarketDataStatisticsReport(self):
		message = buildMessageHeader(self.application, "DP")
		self.assertTrue(isMarketDataStatisticsReport(message))

	#---------------------------------------------------------------------
	# Event Messages
	#---------------------------------------------------------------------

	def test_isBusinessMessageReject(self):
		message = buildMessageHeader(self.application, fix.MsgType_BusinessMessageReject)
		self.assertTrue(isBusinessMessageReject(message))

if __name__ == '__main__':
	unittest.main()