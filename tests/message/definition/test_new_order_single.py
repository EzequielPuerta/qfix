import unittest
import quickfix as fix
import quickfix50sp2 as fix50
from tests.utils import bymaApplication
from qfix.engine.tools import stringDatetimeAsUTC, stringDateAsUTC
from qfix.field.byma.extras import OrderBook, OrderSource, TradeFlag
from qfix.message.types import isNewOrderSingle
from qfix.message.generic import isMessageOfType, getValue, getString, getValueGroup
from qfix.message.definition.new_order_single import NewOrderSingleDefinition

class TestNewOrderSingle(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestNewOrderSingle, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def orderSingleComplete(self):
		jsonMessage = {}
		jsonMessage["35"] = "D"						#MsgType(String)
		jsonMessage["11"] = "0303456"				#ClOrdID(String)
		partyIDsGroup1 = {}
		partyIDsGroup1["448"] = "MIT User"			#PartyID(String)
		partyIDsGroup1["447"] = "D"					#PartyIDSource(Char)
		partyIDsGroup1["452"] = 53					#PartyRole(Int)
		jsonMessage["453"] = [partyIDsGroup1]		#NoPartyIDs(NumInGroup)
		jsonMessage["1"] = "123"					#Account(String)
		jsonMessage["581"] = 1						#AccountType(Int)
		jsonMessage["55"] = "INST"					#Symbol(String)
		jsonMessage["167"] = "CS"					#SecurityType(String)
		jsonMessage["15"] = "ARS"					#Currency(String)
		jsonMessage["30001"] = "4"					#OrderBook(String)
		jsonMessage["40"] = "4"						#OrdType(Char)
		jsonMessage["59"] = "6"						#TimeInForce(Char)
		jsonMessage["126"] = "21/04/1990 12:34:56"	#ExpireTime(UTCTimestamp)
		tradingSessionsGroup1 = {}
		tradingSessionsGroup1["336"] = "a"			#TradingSessionID(String)
		jsonMessage["386"] = [tradingSessionsGroup1]#NoTradingSessions(NumInGroup)
		jsonMessage["18"] = "n"						#ExecInst(MultipleCharValue)
		jsonMessage["54"] = "1"						#Side(Char)
		jsonMessage["38"] = 666666.6				#OrderQty(Qty (Float))
		jsonMessage["1138"] = 666.6					#DisplayQty(Qty (Float))
		jsonMessage["1084"] = "4"					#DisplayMethod(Char)
		jsonMessage["110"] = 333.0					#MinQty(Qty (Float))
		jsonMessage["44"] = 1230.45					#Price(Price (Float))
		jsonMessage["99"] = 123.45					#StopPx(Price (Float))
		jsonMessage["1091"] = "N"					#PreTradeAnonymity(Boolean)
		jsonMessage["528"] = "P"					#OrderCapacity(Char)
		jsonMessage["583"] = "6"					#ClOrdLinkID(String)
		jsonMessage["30004"] = "Order Test"			#OrderSource(String)
		jsonMessage["60"] = "05/06/2020 21:43:05"	#TransactTime(UTCTimestamp)
		jsonMessage["63"] = "3"						#SettlType(String)
		jsonMessage["64"] = "20/05/2020"			#SettlDate(LocalMktDate)
		jsonMessage["29501"] = "1"					#TradeFlag(String)
		return jsonMessage

	def orderSingleJustWithMandatoryFields(self):
		jsonMessage = {}							#								{
		jsonMessage["35"] = "D"						#MsgType(String)					"35"	: "D",
		jsonMessage["11"] = "0303456"				#ClOrdID(String)					"11"	: "0303456",
		partyIDsGroup1 = {}							#									"453"	: [{
		partyIDsGroup1["448"] = "MIT User"			#PartyID(String)								"448"	: "MIT User",
		partyIDsGroup1["447"] = "D"					#PartyIDSource(Char)							"447"	: "D",
		partyIDsGroup1["452"] = 53					#PartyRole(Int)									"452"	: 53
		jsonMessage["453"] = [partyIDsGroup1]		#NoPartyIDs(NumInGroup)						   }],
		jsonMessage["55"] = "INST"					#Symbol(String)						"55"	: "INST",
		jsonMessage["167"] = "CS"					#SecurityType(String)				"167"	: "CS",
		jsonMessage["15"] = "ARS"					#Currency(String)					"15"	: "ARS",
		jsonMessage["40"] = "4"						#OrdType(Char)						"40"	: "4",
		jsonMessage["54"] = "1"						#Side(Char)							"54"	: "1",
		jsonMessage["38"] = 666666.6				#OrderQty(Qty (Float))				"38"	: 666666.6,
		jsonMessage["44"] = 1230.45					#Price(Price (Float))				"44"	: 1230.45,
		jsonMessage["99"] = 123.45					#StopPx(Price (Float))				"99"	: 123.45,
		jsonMessage["60"] = "05/06/2020 21:43:05"	#TransactTime(UTCTimestamp)			"60"	: "05/06/2020 21:43:05",
		jsonMessage["29501"] = "1"					#TradeFlag(String)					"29501"	: "1"
		return jsonMessage							#								}

	def test_newOrderSingle(self):
		jsonMessage = self.orderSingleComplete()
		newOrderMessage = NewOrderSingleBYMADefinition(fix_app=self.application, json_message=jsonMessage).buildFixMessage()

		self.assertTrue(isNewOrderSingle(newOrderMessage)
		self.assertEqual(getValue(newOrderMessage, fix.ClOrdID()), jsonMessage["11"])

		self.assertEqual(getValue(newOrderMessage, fix.NoPartyIDs()), len(jsonMessage["453"]))
		self.assertEqual(1, len(jsonMessage["453"]))
		partyGroup = fix50.NewOrderSingle.NoPartyIDs()
		newOrderMessage.getGroup(1, partyGroup)
		self.assertEqual(getValueGroup(partyGroup, fix.PartyID()), jsonMessage["453"][0]["448"])
		self.assertEqual(getValueGroup(partyGroup, fix.PartyIDSource()), jsonMessage["453"][0]["447"])
		self.assertEqual(getValueGroup(partyGroup, fix.PartyRole()), jsonMessage["453"][0]["452"])

		self.assertEqual(getValue(newOrderMessage, fix.Account()), jsonMessage["1"])
		self.assertEqual(getValue(newOrderMessage, fix.AccountType()), jsonMessage["581"])
		self.assertEqual(getValue(newOrderMessage, fix.Symbol()), jsonMessage["55"])
		self.assertEqual(getValue(newOrderMessage, fix.SecurityType()), jsonMessage["167"])
		self.assertEqual(getValue(newOrderMessage, fix.Currency()), jsonMessage["15"])
		self.assertEqual(getValue(newOrderMessage, OrderBook()), jsonMessage["30001"])
		self.assertEqual(getValue(newOrderMessage, fix.OrdType()), jsonMessage["40"])
		self.assertEqual(getValue(newOrderMessage, fix.TimeInForce()), jsonMessage["59"])
		self.assertEqual(getString(newOrderMessage, fix.ExpireTime()), stringDatetimeAsUTC(jsonMessage["126"]))
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.ExpireDate())

		self.assertEqual(getValue(newOrderMessage, fix.NoTradingSessions()), len(jsonMessage["386"]))
		self.assertEqual(1, len(jsonMessage["386"]))
		sessionGroup = fix50.NewOrderSingle.NoTradingSessions()
		newOrderMessage.getGroup(1, sessionGroup)
		self.assertEqual(getValueGroup(sessionGroup, fix.TradingSessionID()), jsonMessage["386"][0]["336"])

		self.assertEqual(getValue(newOrderMessage, fix.ExecInst()), jsonMessage["18"])
		self.assertEqual(getValue(newOrderMessage, fix.Side()), jsonMessage["54"])
		self.assertEqual(getValue(newOrderMessage, fix.OrderQty()), jsonMessage["38"])
		self.assertEqual(getValue(newOrderMessage, fix.DisplayQty()), jsonMessage["1138"])
		self.assertEqual(getValue(newOrderMessage, fix.DisplayMethod()), jsonMessage["1084"])
		self.assertEqual(getValue(newOrderMessage, fix.MinQty()), jsonMessage["110"])
		self.assertEqual(getValue(newOrderMessage, fix.Price()), jsonMessage["44"])
		self.assertEqual(getValue(newOrderMessage, fix.StopPx()), jsonMessage["99"])
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.PegOffsetValue())
		self.assertFalse(getValue(newOrderMessage, fix.PreTradeAnonymity()))
		self.assertEqual(getValue(newOrderMessage, fix.OrderCapacity()), jsonMessage["528"])
		self.assertEqual(getValue(newOrderMessage, fix.ClOrdLinkID()), jsonMessage["583"])
		self.assertEqual(getValue(newOrderMessage, OrderSource()), jsonMessage["30004"])
		self.assertEqual(getString(newOrderMessage, fix.TransactTime()), stringDatetimeAsUTC(jsonMessage["60"]))
		self.assertEqual(getValue(newOrderMessage, fix.SettlType()), jsonMessage["63"])
		self.assertEqual(getString(newOrderMessage, fix.SettlDate()), stringDateAsUTC(jsonMessage["64"]))
		self.assertEqual(getValue(newOrderMessage, TradeFlag()), jsonMessage["29501"])

	def test_newOrderSingleJustWithMandatoryFields(self):
		jsonMessage = self.orderSingleJustWithMandatoryFields()
		newOrderMessage = NewOrderSingleBYMADefinition(fix_app=self.application, json_message=jsonMessage).buildFixMessage()

		self.assertTrue(isNewOrderSingle(newOrderMessage)
		self.assertEqual(getValue(newOrderMessage, fix.ClOrdID()), jsonMessage["11"])

		self.assertEqual(getValue(newOrderMessage, fix.NoPartyIDs()), len(jsonMessage["453"]))
		self.assertEqual(1, len(jsonMessage["453"]))
		partyGroup = fix50.NewOrderSingle.NoPartyIDs()
		newOrderMessage.getGroup(1, partyGroup)
		self.assertEqual(getValueGroup(partyGroup, fix.PartyID()), jsonMessage["453"][0]["448"])
		self.assertEqual(getValueGroup(partyGroup, fix.PartyIDSource()), jsonMessage["453"][0]["447"])
		self.assertEqual(getValueGroup(partyGroup, fix.PartyRole()), jsonMessage["453"][0]["452"])

		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.Account())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.AccountType())
		self.assertEqual(getValue(newOrderMessage, fix.Symbol()), jsonMessage["55"])
		self.assertEqual(getValue(newOrderMessage, fix.SecurityType()), jsonMessage["167"])
		self.assertEqual(getValue(newOrderMessage, fix.Currency()), jsonMessage["15"])
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, OrderBook())
		self.assertEqual(getValue(newOrderMessage, fix.OrdType()), jsonMessage["40"])
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.TimeInForce())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.ExpireTime())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.ExpireDate())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.NoTradingSessions())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.TradingSessionID())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.ExecInst())
		self.assertEqual(getValue(newOrderMessage, fix.Side()), jsonMessage["54"])
		self.assertEqual(getValue(newOrderMessage, fix.OrderQty()), jsonMessage["38"])
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.DisplayQty())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.DisplayMethod())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.MinQty())
		self.assertEqual(getValue(newOrderMessage, fix.Price()), jsonMessage["44"])
		self.assertEqual(getValue(newOrderMessage, fix.StopPx()), jsonMessage["99"])
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.PegOffsetValue())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.PreTradeAnonymity())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.OrderCapacity())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.ClOrdLinkID())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, OrderSource())
		self.assertEqual(getString(newOrderMessage, fix.TransactTime()), stringDatetimeAsUTC(jsonMessage["60"]))
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.SettlType())
		with self.assertRaises(fix.FieldNotFound):
			getValue(newOrderMessage, fix.SettlDate())
		self.assertEqual(getValue(newOrderMessage, TradeFlag()), jsonMessage["29501"])

if __name__ == '__main__':
	unittest.main()