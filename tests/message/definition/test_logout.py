import unittest
import quickfix as fix
from tests.utils import bymaApplication
from qfix.message.types import isLogout
from qfix.message.generic import getValue
from qfix.message.definition.logout import LogoutDefinition

class TestLogout(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestLogout, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def test_logoutFromAppLevel(self):
		logoutReason = "Good Bye Cruel World!"
		jsonMessage = {}
		jsonMessage["35"] = "5"
		jsonMessage["58"] = logoutReason
		message = LogoutDefinition(fix_app=self.application, json_message=jsonMessage).buildFixMessage()
		self.assertTrue(isLogout(message))
		self.assertEqual(getValue(message, fix.Text()), logoutReason)

if __name__ == '__main__':
	unittest.main()