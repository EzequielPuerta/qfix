import unittest
import quickfix as fix
import quickfix50sp2 as fix50
from tests.utils import bymaApplication
from qfix.message.types import isMarketDataRequest
from qfix.message.definition.market_data_request import MarketDataRequestDefinition

class TestMarketDataRequest(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestMarketDataRequest, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def marketDataRequest(self):
		jsonMessage = {}							#								{
		jsonMessage["35"] = "V"						#MsgType(String)					"35"	: "V",
		jsonMessage["262"] = "13245"				#MDReqId(String)					"262"	: "12345",
        jsonMessage["263"] = "0"                    #SubscriptionRequestType(Char)		"263"	: "0",
        jsonMessage["264"] = 5                      #MarketDepth(Int)					"264"	: 5,
        jsonMessage["265"] = 1                      #MDUpdateType(Int)					"265"	: 1,
        jsonMessage["266"] = "Y"                    #AggregatedBook(Boolean)			"266"	: "Y",
		entryGroup1 = {}							#									"267"	: [{
		entryGroup1["269"] = "0"		        	#MDEntryType(Char)								"269"	: "0"
        entryGroup2 = {}							#												},{
		entryGroup2["269"] = "1"        			#MDEntryType(Char)								"269"	: "1"
        entryGroup3 = {}							#												},{
		entryGroup3["269"] = "2"			        #MDEntryType(Char)								"269"	: "2"
		jsonMessage["267"] = [entryGroup1,			#												}],
                                entryGroup2,		#								
                                entryGroup3]		#NoMDEntryTypes(NumInGroup)		
        relatedSymGroup1 = {}						#									"146"	: [{
        relatedSymGroup1["55"] = "A"                #Symbol(String)									"55"	: "A"
        relatedSymGroup1["15"] = "ARS"				#Currency(String)								"15"	: "ARS"
		jsonMessage["146"] = [relatedSymGroup1]     #NoRelatedSym(NumInGroup)						}]
		return jsonMessage							#								}

	def test_marketDataRequest(self):
		jsonMessage = self.marketDataRequest()
		marketDataMessage = MarketDataRequestDefinition(fix_app=self.application, json_message=jsonMessage).buildFixMessage()

		self.assertTrue(isMarketDataRequest(marketDataMessage)
		self.assertEqual(getValue(marketDataMessage, fix.MDReqId()), jsonMessage["262"])
		self.assertEqual(getValue(marketDataMessage, fix.SubscriptionRequestType()), jsonMessage["263"])
        self.assertEqual(getValue(marketDataMessage, fix.MarketDepth()), jsonMessage["264"])
        self.assertEqual(getValue(marketDataMessage, fix.MDUpdateType()), jsonMessage["265"])
        self.assertTrue(getValue(marketDataMessage, fix.AggregatedBook()))

        self.assertEqual(getValue(marketDataMessage, fix.NoMDEntryTypes()), len(jsonMessage["453"]))
		self.assertEqual(3, len(jsonMessage["453"]))
		entryGroup = fix50.MarketDataRequest.NoMDEntryTypes()
		marketDataMessage.getGroup(1, entryGroup)
		self.assertEqual(getValueGroup(entryGroup, fix.MDEntryType()), jsonMessage["453"][0]["269"])
        marketDataMessage.getGroup(2, entryGroup)
		self.assertEqual(getValueGroup(entryGroup, fix.MDEntryType()), jsonMessage["453"][1]["269"])
        marketDataMessage.getGroup(3, entryGroup)
		self.assertEqual(getValueGroup(entryGroup, fix.MDEntryType()), jsonMessage["453"][2]["269"])

        self.assertEqual(getValue(marketDataMessage, fix.NoRelatedSym()), len(jsonMessage["146"]))
		self.assertEqual(1, len(jsonMessage["146"]))
		relatedSymGroup = fix50.MarketDataRequest.NoRelatedSym()
		marketDataMessage.getGroup(1, relatedSymGroup)
		self.assertEqual(getValueGroup(relatedSymGroup, fix.Symbol()), jsonMessage["146"][0]["55"])
        self.assertEqual(getValueGroup(relatedSymGroup, fix.Currency()), jsonMessage["146"][0]["15"])

if __name__ == '__main__':
	unittest.main()