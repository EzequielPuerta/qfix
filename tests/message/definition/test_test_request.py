import unittest
import quickfix as fix
from tests.utils import bymaApplication
from qfix.message.types import isTestRequest
from qfix.message.generic import getValue
from qfix.message.definition.test_request import TestRequestDefinition

class TestTestRequest(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestTestRequest, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def test_testRequestFromAppLevel(self):
		testReqID = "Hey server! Send me a heartbeat message!"
		jsonMessage = {}
		jsonMessage["35"] = "1"
		jsonMessage["112"] = testReqID
		message = TestRequestDefinition(fix_app=self.application, json_message=jsonMessage).buildFixMessage()
		self.assertTrue(isTestRequest(message)
		self.assertEqual(getValue(message, fix.TestReqID()), testReqID)

if __name__ == '__main__':
	unittest.main()