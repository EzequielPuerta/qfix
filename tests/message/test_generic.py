import unittest
import quickfix as fix
import quickfix50sp2 as fix50
from tests.utils import bymaApplication
from qfix.message.generic import *

class TestGenericMessages(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestGenericMessages, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def test_buildMessageHeaderToSendToMillenium(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)

		self.assertEqual(getHeaderValue(message, fix.BeginString()), fix.BeginString_FIXT11)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.SenderCompID()), self.application.senderCompID)
		self.assertEqual(getHeaderValue(message, fix.TargetCompID()), self.application.targetCompID)
		self.assertEqual(getHeaderValue(message, fix.DeliverToCompID()), 'FGW')

	def test_buildMessageHeaderToSendToFutures(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon, forMillenium=False)

		self.assertEqual(getHeaderValue(message, fix.BeginString()), fix.BeginString_FIXT11)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.SenderCompID()), self.application.senderCompID)
		self.assertEqual(getHeaderValue(message, fix.TargetCompID()), self.application.targetCompID)
		self.assertEqual(getHeaderValue(message, fix.DeliverToCompID()), 'UMDF')

	def test_buildMessageHeaderAsReceivedFromMillenium(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon, asReceived=True)

		self.assertEqual(getHeaderValue(message, fix.BeginString()), fix.BeginString_FIXT11)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.SenderCompID()), self.application.targetCompID)
		self.assertEqual(getHeaderValue(message, fix.TargetCompID()), self.application.senderCompID)
		self.assertEqual(getHeaderValue(message, fix.DeliverToCompID()), 'FGW')

	def test_buildMessageHeaderAsReceivedFromFutures(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon, asReceived=True, forMillenium=False)

		self.assertEqual(getHeaderValue(message, fix.BeginString()), fix.BeginString_FIXT11)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.SenderCompID()), self.application.targetCompID)
		self.assertEqual(getHeaderValue(message, fix.TargetCompID()), self.application.senderCompID)
		self.assertEqual(getHeaderValue(message, fix.DeliverToCompID()), 'UMDF')

	def test_isMessageOfType(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertTrue(isMessageOfType(message, fix.MsgType_Logon))

	def test_hasField(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertFalse(hasField(message, fix.Text()))
		message.setField(fix.Text('Test'))
		self.assertTrue(hasField(message, fix.Text()))
		self.assertEqual(getBodyValue(message, fix.Text()), 'Test')

	def test_getValue(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), getValue(message, fix.MsgType()))

		self.assertEqual(getValue(message, fix.Username()), None)
		with self.assertRaises(fix.FieldNotFound):
			getBodyValue(message, fix.Username())
		message.setField(fix.Username(self.application.username))
		self.assertEqual(getBodyValue(message, fix.Username()), self.application.username)
		self.assertEqual(getBodyValue(message, fix.Username()), getValue(message, fix.Username()))

		checkSum = 100
		message.getTrailer().setField(fix.CheckSum(checkSum))
		self.assertEqual(getTrailerValue(message, fix.CheckSum()), checkSum)
		self.assertEqual(getTrailerValue(message, fix.CheckSum()), getValue(message, fix.CheckSum()))

	def test_getBodyValue(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		message.setField(fix.Username(self.application.username))
		self.assertEqual(getBodyValue(message, fix.Username()), self.application.username)

	def test_getBodyString(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		message.setField(fix.Username(self.application.username))
		self.assertEqual(getBodyString(message, fix.Username()), self.application.username)

	def test_getHeaderValue(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertEqual(getHeaderValue(message, fix.MsgType()), fix.MsgType_Logon)

	def test_getHeaderString(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		self.assertFalse(hasField(message.getHeader(), fix.MsgSeqNum()))
		message.getHeader().setField(fix.MsgSeqNum(123))
		self.assertTrue(hasField(message.getHeader(), fix.MsgSeqNum()))
		self.assertEqual(getHeaderValue(message, fix.MsgSeqNum()), 123)
		self.assertEqual(getHeaderString(message, fix.MsgSeqNum()), '123')

	def test_getTrailerValue(self):
		checkSum = 100
		message = buildMessageHeader(self.application, fix.MsgType_Logon)
		message.getTrailer().setField(fix.CheckSum(checkSum))
		self.assertEqual(getTrailerValue(message, fix.CheckSum()), checkSum)

	def test_getValueGroup(self):
		message = buildMessageHeader(self.application, fix.MsgType_NewOrderSingle)
		self.assertFalse(hasField(message, fix.NoPartyIDs()))
		groupToSet = fix50.NewOrderSingle.NoPartyIDs()
		groupToSet.setField(fix.PartyID('TestID'))
		message.addGroup(groupToSet)

		self.assertTrue(hasField(message, fix.NoPartyIDs()))
		self.assertEqual(getBodyValue(message, fix.NoPartyIDs()), 1)
		groupToGet = fix50.NewOrderSingle.NoPartyIDs()
		message.getGroup(1, groupToGet)
		self.assertEqual(getValueGroup(groupToGet, fix.PartyID()), 'TestID')

	def test_getStringGroup(self):
		message = buildMessageHeader(self.application, fix.MsgType_NewOrderSingle)
		self.assertFalse(hasField(message, fix.NoPartyIDs()))
		groupToSet = fix50.NewOrderSingle.NoPartyIDs()
		groupToSet.setField(fix.PartyRole(53))
		message.addGroup(groupToSet)

		self.assertTrue(hasField(message, fix.NoPartyIDs()))
		self.assertEqual(getBodyValue(message, fix.NoPartyIDs()), 1)
		groupToGet = fix50.NewOrderSingle.NoPartyIDs()
		message.getGroup(1, groupToGet)
		self.assertEqual(getStringGroup(groupToGet, fix.PartyRole()), '53')

if __name__ == '__main__':
	unittest.main()