import unittest
from abc import ABC, abstractmethod
from qfix.engine.suitable_class_finder import SuitableClassFinder

# 	HIERARCHY					|	ATTRIBUTES				|	CONCRETE
#=========================================================================================
# Relative (Abstract)
#	|
#	|___ Grandfather				{name: Joe, age: 70}
#			|
#			|___ Aunt				{name: Jenny, age: 38}		X
#			|
#			|___ Uncle				{name: John, age: 45}
#			|		|
#			|		|___ Cousin 	{name: Jenny, age: 16}		X
#			|
#			|___ Father 			{name: Jack, age: 40}
#					|
#					|___ Me 		{name: John, age: 18}		X
#					|
#					|___ Brother	{name: Jeremy, age: 10}		X
#					|
#					|___ Sister		{name: Jenny, age: 14}		X

class Relative(ABC):
	name = ""
	age = 0

	@classmethod
	def canHandle(cls, expectedName):
		return cls.name == expectedName

	@classmethod
	def canHandleNameAndAge(cls, expectedName, expectedAge):
		return (cls.name == expectedName) and (cls.age == expectedAge)

class Nobody():
	name = "NN"
	age = -1

class Grandfather(Relative):
	name = "Joe"
	age = 70

class Father(Grandfather):
	name = "Jack"
	age = 40

class Uncle(Grandfather):
	name = "John"
	age = 45

class Aunt(Grandfather):
	name = "Jenny"
	age = 38

class Cousin(Uncle):
	name = "Jenny"
	age = 16

class Me(Father):
	name = "John"
	age = 18

class Brother(Father):
	name = "Jeremy"
	age = 10

class Sister(Father):
	name = "Jenny"
	age = 14

class TestSuitableClassFinder(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestSuitableClassFinder, self).__init__(*args, **kwargs)

	def test_canHandle(self):
		# ONLY concrete subclasses can handle the suitable object
		self.assertEqual(Me, SuitableClassFinder(Relative).suitableFor("John"))

		# NO MORE THAN ONE concrete subclass can handle the suitable object
		with self.assertRaises(ValueError):
			SuitableClassFinder(Relative).suitableFor("Jenny")

		# JUST ONE concrete subclass should handle the suitable object
		with self.assertRaises(ValueError):
			SuitableClassFinder(Relative).suitableFor("Bob")

	def test_defaultSubclass(self):
		# You can define a default class
		self.assertEqual(Nobody, SuitableClassFinder(Relative).suitableFor("Bob", defaultSubclass=Nobody))

	def test_suitableMethod(self):
		# You can specify an explicit canHandle method (useful for multiple positional arguments)
		self.assertEqual(Sister, SuitableClassFinder(Relative).suitableFor("Jenny", 14, suitableMethod='canHandleNameAndAge'))