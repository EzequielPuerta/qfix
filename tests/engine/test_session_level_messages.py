import unittest
import quickfix as fix
from tests.utils import bymaApplication
from qfix.engine.session_level_messages import *
from qfix.message.generic import buildMessageHeader, isMessageOfType, getHeaderValue, getBodyValue

class TestSessionLevelMessages(unittest.TestCase):

	def __init__(self, *args, **kwargs):
		super(TestSessionLevelMessages, self).__init__(*args, **kwargs)
		self.application = bymaApplication()

	def test_logon(self):
		message = buildMessageHeader(self.application, fix.MsgType_Logon)

		self.assertTrue(isMessageOfType(message, fix.MsgType_Logon))
		with self.assertRaises(fix.FieldNotFound):
			getBodyValue(message, fix.Username())
		with self.assertRaises(fix.FieldNotFound):
			getBodyValue(message, fix.Password())

		logon(self.application, message)

		self.assertTrue(isMessageOfType(message, fix.MsgType_Logon))
		self.assertEqual(
			getBodyValue(message, fix.Username()),
			self.application.username)
		self.assertEqual(
			getBodyValue(message, fix.Password()),
			self.application.password)

	def test_logout(self):
		message = logout(self.application)
		self.assertTrue(isMessageOfType(message, fix.MsgType_Logout))
		self.assertEqual(getBodyValue(message, fix.Text()), "Manual disconnection")

	def test_testRequestHeartbeat(self):
		testRequest = buildMessageHeader(self.application, fix.MsgType_TestRequest, asReceived=True)
		testRequest.setField(fix.TestReqID("Hello World!"))
		heartbeatResponse = testRequestHeartbeat(self.application, testRequest)
		self.assertTrue(isMessageOfType(heartbeatResponse, fix.MsgType_Heartbeat))
		self.assertEqual(getHeaderValue(testRequest, fix.SenderCompID()), getHeaderValue(heartbeatResponse, fix.TargetCompID()))
		self.assertEqual(getHeaderValue(testRequest, fix.TargetCompID()), getHeaderValue(heartbeatResponse, fix.SenderCompID()))
		self.assertEqual(getBodyValue(testRequest, fix.TestReqID()), getBodyValue(heartbeatResponse, fix.TestReqID()))
		self.assertEqual("Hello World!", getBodyValue(heartbeatResponse, fix.TestReqID()))

	def test_testRequest(self):
		content = "Hello World!"
		message = testRequest(self.application, content)
		self.assertTrue(isMessageOfType(message, fix.MsgType_TestRequest))
		self.assertEqual(getBodyValue(message, fix.TestReqID()), content)

if __name__ == '__main__':
	unittest.main()