from qfix.message.generic import isMessageOfType, getValue, getValueGroup

# Asserts
def assertIsMessageOfType(testCase, message, fixMessageType):
	testCase.assertTrue(isMessageOfType(message, fixMessageType))

def assertValueEqualTo(testCase, message, fixField, expectedValue):
	testCase.assertEqual(getValue(message, fixField), expectedValue)

def assertValueIsTrue(testCase, message, fixField):
	testCase.assertTrue(getValue(message, fixField))

def assertValueIsFalse(testCase, message, fixField):
	testCase.assertFalse(getValue(message, fixField))

def assertValueGroupEqualTo(testCase, message, group, indexGroup, fixField, expectedValue):
	message.getGroup(indexGroup, group)
	testCase.assertEqual(getValueGroup(group, fixField), expectedValue)

def assertValueGroupAreEqualTo(testCase, message, fixGroup, expectedValuesByField):
	for index in range(1, len(expectedValuesByField)+1):
		for each in expectedValuesByField:
			assertValueGroupEqualTo(testCase, message, fixGroup, index, each[0], each[1])

# Mock Application
class MockApplication():

	def __init__(self, target, sender, username, password, account):
		super().__init__()
		self.targetCompID = target
		self.senderCompID = sender
		self.username = username
		self.password = password
		self.account = account

def bymaApplication():
	return MockApplication('STUN', 'dmax199cb', 'dmx199-80', 'nueva123', 'STUN')