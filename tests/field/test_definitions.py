import unittest
import quickfix as fix
from datetime import datetime
from qfix.field.definitions import *
from qfix.message.adapter import MessageAdapter

#####################################################################################################################
class TestFieldDefinition(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		self.emptyJsonMessage = MessageAdapter.concerning({})
		self.jsonMessage = MessageAdapter.concerning({"58":'Hi'})
		self.invalidJsonMessage = MessageAdapter.concerning({"58":123})

		self.emptyFixMessage = MessageAdapter.concerning(fix.Message())
		self.fixMessage = fix.Message()
		self.fixMessage.setField(fix.Text('Bye'))
		self.fixMessage = MessageAdapter.concerning(self.fixMessage)

		super(TestFieldDefinition, self).__init__(*args, **kwargs)

	def test_cantInstantiateAbstracClassFieldDefinition(self):
		with self.assertRaises(TypeError):
			FieldDefinition(field=fix.Text())
		with self.assertRaises(TypeError):
			BasicFieldDefinition(field=fix.Text())

	def test_fieldName(self):
		definition = StringFieldDefinition(field=fix.MsgType())
		self.assertTrue(definition.fieldName(), 'MsgType(35)')

	def test_fieldID(self):
		definition = StringFieldDefinition(field=fix.MsgType())
		self.assertTrue(definition.fieldID(), '35')

	def test_getRequiredAndPresentValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=True)
		self.assertEqual(definition.getValueOn(self.jsonMessage), 'Hi')
		self.assertEqual(definition.getValueOn(self.fixMessage), 'Bye')

	def test_getNotRequiredAndPresentValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=False)
		self.assertEqual(definition.getValueOn(self.jsonMessage), 'Hi')
		self.assertEqual(definition.getValueOn(self.fixMessage), 'Bye')

	def test_getRequiredAndNotPresentValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=True)
		with self.assertRaises(ValueError):
			definition.getValueOn(self.emptyJsonMessage)
		with self.assertRaises(ValueError):
			definition.getValueOn(self.emptyFixMessage)

	def test_getNotRequiredAndNotPresentValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=False)
		self.assertIsNone(definition.getValueOn(self.emptyJsonMessage))
		self.assertIsNone(definition.getValueOn(self.emptyFixMessage))

	def test_getRequiredAndInvalidValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=True)
		with self.assertRaises(ValueError):
			definition.getValueOn(self.invalidJsonMessage)

	def test_getNotRequiredAndInvalidValue(self):
		definition = StringFieldDefinition(field=fix.Text(), required=False)
		self.assertIsNone(definition.getValueOn(self.invalidJsonMessage))

#####################################################################################################################
class TestStringFieldDefinition(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestStringFieldDefinition, self).__init__(*args, **kwargs)

	def test_stringFieldDefinitionWithDifferentFixFields(self):
		self.assertIsInstance(
			StringFieldDefinition(field=fix.Text()),
			StringFieldDefinition)								# String
		self.assertIsInstance(
			StringFieldDefinition(field=fix.Side()),
			StringFieldDefinition)								# Char
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.AccountType())		# Integer
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.Price())			# Float
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.ResetSeqNumFlag())	# Bool
		self.assertIsInstance(
			StringFieldDefinition(field=fix.ExpireDate()),
			StringFieldDefinition)								# String date
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.TransactTime())		# UTC Timestamp
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.MDEntryDate())		# UTC Date
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.MDEntryTime())		# UTC Time
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.CheckSum())			# CheckSum
		self.assertIsInstance(
			StringFieldDefinition(field=fix.ExecInst()),
			StringFieldDefinition)								# Space separated
		with self.assertRaises(ValueError):
			StringFieldDefinition(field=fix.NoPartyIDs())		# Grouped data

	def test_defaultStringFieldDefinitionCreation(self):
		field = fix.Text()
		definition = StringFieldDefinition(field=field)
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertEqual(len(definition.values), 0)

	def test_requiredStringFieldDefinitionCreation(self):
		field = fix.Text()
		definition = StringFieldDefinition(field=field, required=True, values=['A', '1'])
		self.assertEqual(definition.fixField, field)
		self.assertTrue(definition.required)
		self.assertIn('A', definition.values)
		self.assertIn('1', definition.values)

	def test_notRequiredStringFieldDefinitionCreation(self):
		field = fix.Text()
		definition = StringFieldDefinition(field=field, required=False, values=['B', '2'])
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertIn('B', definition.values)
		self.assertIn('2', definition.values)

	def test_stringValuesBehavior(self):
		definition = StringFieldDefinition(field=fix.Text())
		self.assertFalse(definition.expectsValues())
		self.assertTrue(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue('B'))
		self.assertTrue(definition.isValidValue('C'))
		self.assertFalse(definition.isValidValue(1))
		self.assertFalse(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

		definition = StringFieldDefinition(field=fix.Text(), values=['A','B'])
		self.assertTrue(definition.expectsValues())
		self.assertTrue(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue('B'))
		self.assertFalse(definition.isValidValue('C'))
		self.assertFalse(definition.isValidValue(1))
		self.assertFalse(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

#####################################################################################################################
class TestIntegerFieldDefinition(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestIntegerFieldDefinition, self).__init__(*args, **kwargs)

	def test_integerFieldDefinitionWithDifferentFixFields(self):
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.Text())			# String
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.Side())			# Char
		self.assertIsInstance(
			IntegerFieldDefinition(field=fix.AccountType()),
			IntegerFieldDefinition)								# Integer
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.Price())			# Float
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.ResetSeqNumFlag())	# Bool
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.ExpireDate())		# String date
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.TransactTime())	# UTC Timestamp
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.MDEntryDate())		# UTC Date
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.MDEntryTime())		# UTC Time
		self.assertIsInstance(
			IntegerFieldDefinition(field=fix.CheckSum()),
			IntegerFieldDefinition)								# CheckSum
		with self.assertRaises(ValueError):
			IntegerFieldDefinition(field=fix.ExecInst())		# Space separated
		self.assertIsInstance(
			IntegerFieldDefinition(field=fix.NoPartyIDs()),
			IntegerFieldDefinition)								# Grouped data

	def test_defaultIntegerFieldDefinitionCreation(self):
		field = fix.AccountType()
		definition = IntegerFieldDefinition(field=field)
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertEqual(len(definition.values), 0)

	def test_requiredIntegerFieldDefinitionCreation(self):
		field = fix.AccountType()
		definition = IntegerFieldDefinition(field=field, required=True, values=[1, 2])
		self.assertEqual(definition.fixField, field)
		self.assertTrue(definition.required)
		self.assertIn(1, definition.values)
		self.assertIn(2, definition.values)

	def test_notRequiredIntegerFieldDefinitionCreation(self):
		field = fix.AccountType()
		definition = IntegerFieldDefinition(field=field, required=False, values=[1, 2])
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertIn(1, definition.values)
		self.assertIn(2, definition.values)

	def test_integerValuesBehavior(self):
		definition = IntegerFieldDefinition(field=fix.AccountType())
		self.assertFalse(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue(1))
		self.assertTrue(definition.isValidValue(2))
		self.assertTrue(definition.isValidValue(3))
		self.assertFalse(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

		definition = IntegerFieldDefinition(field=fix.AccountType(), values=[1, 2])
		self.assertTrue(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue(1))
		self.assertTrue(definition.isValidValue(2))
		self.assertFalse(definition.isValidValue(3))
		self.assertFalse(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

#####################################################################################################################
class TestFloatFieldDefinition(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestFloatFieldDefinition, self).__init__(*args, **kwargs)

	def test_floatFieldDefinitionWithDifferentFixFields(self):
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.Text())				# String
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.Side())				# Char
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.AccountType())		# Integer
		self.assertIsInstance(
			FloatFieldDefinition(field=fix.Price()),
			FloatFieldDefinition)								# Float
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.ResetSeqNumFlag())	# Bool
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.ExpireDate())		# String date
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.TransactTime())		# UTC Timestamp
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.MDEntryDate())		# UTC Date
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.MDEntryTime())		# UTC Time
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.CheckSum())			# CheckSum
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.ExecInst())			# Space separated
		with self.assertRaises(ValueError):
			FloatFieldDefinition(field=fix.NoPartyIDs())		# Grouped data

	def test_defaultFloatFieldDefinitionCreation(self):
		field = fix.Price()
		definition = FloatFieldDefinition(field=field)
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertEqual(len(definition.values), 0)

	def test_requiredFloatFieldDefinitionCreation(self):
		field = fix.Price()
		definition = FloatFieldDefinition(field=field, required=True, values=[1, 2.5])
		self.assertEqual(definition.fixField, field)
		self.assertTrue(definition.required)
		self.assertIn(1, definition.values)
		self.assertIn(2.5, definition.values)

	def test_notRequiredFloatFieldDefinitionCreation(self):
		field = fix.Price()
		definition = FloatFieldDefinition(field=field, required=False, values=[1, 2.5])
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertIn(1, definition.values)
		self.assertIn(2.5, definition.values)

	def test_floatValuesBehavior(self):
		definition = FloatFieldDefinition(field=fix.Price())
		self.assertFalse(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue(1))
		self.assertTrue(definition.isValidValue(1.0))
		self.assertTrue(definition.isValidValue(2.5))
		self.assertTrue(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

		definition = FloatFieldDefinition(field=fix.Price(), values=[1, 2.5])
		self.assertTrue(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertTrue(definition.isValidValue(1))
		self.assertTrue(definition.isValidValue(1.0))
		self.assertTrue(definition.isValidValue(2.5))
		self.assertFalse(definition.isValidValue(3.1415))
		self.assertFalse(definition.isValidValue(True))

#####################################################################################################################
class TestBooleanFieldDefinition(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestBooleanFieldDefinition, self).__init__(*args, **kwargs)

	def test_booleanFieldDefinitionWithDifferentFixFields(self):
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.Text())				# String
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.Side())				# Char
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.AccountType())			# Integer
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.Price())				# Float
		self.assertIsInstance(
			BooleanFieldDefinition(field=fix.ResetSeqNumFlag()),
			BooleanFieldDefinition)									# Bool
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.ExpireDate())			# String date
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.TransactTime())		# UTC Timestamp
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.MDEntryDate())			# UTC Date
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.MDEntryTime())			# UTC Time
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.CheckSum())			# CheckSum
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.ExecInst())			# Space separated
		with self.assertRaises(ValueError):
			BooleanFieldDefinition(field=fix.NoPartyIDs())			# Grouped data

	def test_defaultBooleanFieldDefinitionCreation(self):
		field = fix.ResetSeqNumFlag()
		definition = BooleanFieldDefinition(field=field)
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertEqual(len(definition.values), 2)
		self.assertIn(True, definition.values)
		self.assertIn(False, definition.values)

	def test_requiredBooleanFieldDefinitionCreation(self):
		field = fix.ResetSeqNumFlag()
		definition = BooleanFieldDefinition(field=field, required=True)
		self.assertEqual(definition.fixField, field)
		self.assertTrue(definition.required)
		self.assertIn(True, definition.values)
		self.assertIn(False, definition.values)

	def test_notRequiredBooleanFieldDefinitionCreation(self):
		field = fix.ResetSeqNumFlag()
		definition = BooleanFieldDefinition(field=field, required=False)
		self.assertEqual(definition.fixField, field)
		self.assertFalse(definition.required)
		self.assertIn(True, definition.values)
		self.assertIn(False, definition.values)

	def test_booleanValuesBehavior(self):
		definition = BooleanFieldDefinition(field=fix.ResetSeqNumFlag())
		self.assertTrue(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertFalse(definition.isValidValue(1))
		self.assertFalse(definition.isValidValue(2.5))
		self.assertTrue(definition.isValidValue(True))
		self.assertTrue(definition.isValidValue(False))

		definition = BooleanFieldDefinition(field=fix.ResetSeqNumFlag(), values=[1, 2.5])
		self.assertTrue(definition.expectsValues())
		self.assertFalse(definition.isValidValue('A'))
		self.assertFalse(definition.isValidValue(1))
		self.assertFalse(definition.isValidValue(2.5))
		self.assertTrue(definition.isValidValue(True))
		self.assertTrue(definition.isValidValue(False))

if __name__ == '__main__':
	unittest.main()