import sys
import time
import threading
import argparse
import quickfix as fix
from tools.echo import echo

class Application(fix.Application):
	@echo
	def onCreate(self, sessionID): return
	@echo
	def onLogon(self, sessionID): return
	@echo
	def onLogout(self, sessionID): return
	@echo
	def toAdmin(self, sessionID, message): return
	@echo
	def fromAdmin(self, sessionID, message): return
	@echo
	def toApp(self, sessionID, message): return
	@echo
	def fromApp(self, message, sessionID): return

def main(file_name):

	try:
		settings = fix.SessionSettings(file_name)
		application = Application()
		storeFactory = fix.FileStoreFactory( settings )
		logFactory = fix.FileLogFactory( settings )
		print('creating acceptor')
		acceptor = fix.SocketAcceptor( application, storeFactory, settings, logFactory )
		print('starting acceptor')
		acceptor.start()

		while 1:
			time.sleep(1)
	except (fix.ConfigError, fix.RuntimeError) as e:
		print(e)

if __name__=='__main__':
	parser = argparse.ArgumentParser(description='FIX Server')
	parser.add_argument('file_name', type=str, help='Name of configuration file')
	args = parser.parse_args()
	main(args.file_name)
