# No uso alpine ni slim-buster
# alpine suele traer problemas con las dependencias
# slim no tiene gcc y demas, y python suele requerirlos
FROM python:3.8-buster

ARG GROUP_ID
ARG USER_ID
ARG TEST_SERVER_PATH=/opt/mercap/test_server
ENV TEST_SERVER_PATH $TEST_SERVER_PATH

RUN groupadd -g $GROUP_ID mcp_users &&\
    useradd --no-log-init -r -u $USER_ID -g mcp_users mercap &&\
    install -d -m 0755 -o mercap -g mcp_users /home/mercap &&\
    chown --changes --silent --no-dereference --recursive $USER_ID:$GROUP_ID /home/mercap

RUN pip install quickfix~=1.15

RUN set -eu; \
    mkdir -p \
        $TEST_SERVER_PATH/logs/ \
        $TEST_SERVER_PATH/sessions/

VOLUME $TEST_SERVER_PATH/conf/
VOLUME $TEST_SERVER_PATH/logs/
VOLUME $TEST_SERVER_PATH/sessions/

COPY conf/ $TEST_SERVER_PATH/conf/
COPY main/ $TEST_SERVER_PATH/main/
COPY run.sh $TEST_SERVER_PATH/run.sh

RUN chown -R mercap:mcp_users $TEST_SERVER_PATH/logs &&\
    chown -R mercap:mcp_users $TEST_SERVER_PATH/sessions &&\
    chown -R mercap:mcp_users $TEST_SERVER_PATH

RUN chmod u=rwx,g=rw $TEST_SERVER_PATH/logs &&\
    chmod u=rwx,g=rw $TEST_SERVER_PATH/sessions &&\
    chmod +x $TEST_SERVER_PATH/run.sh

USER mercap
WORKDIR $TEST_SERVER_PATH
ENTRYPOINT ["./run.sh"]
