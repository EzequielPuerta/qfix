#!/bin/bash

cd $QFIX_PATH
CONFIG_FILE=conf.cfg

cp $QFIX_PATH/conf/$CONFIG_TMP_FILE $QFIX_PATH/$CONFIG_FILE
eval "echo \"$(cat $QFIX_PATH/$CONFIG_FILE)\" > $QFIX_PATH/$CONFIG_FILE"

cp $QFIX_PATH/qfix/rabbitmq_client/conf.py.tmp $QFIX_PATH/qfix/rabbitmq_client/conf.py
eval "echo \"$(cat $QFIX_PATH/qfix/rabbitmq_client/conf.py)\" > $QFIX_PATH/qfix/rabbitmq_client/conf.py"

eval python -u qfix_client.py $CONFIG_FILE