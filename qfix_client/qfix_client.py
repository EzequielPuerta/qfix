import time
import signal
import sys
import argparse
import os
import asyncio
import logging

import quickfix as fix
from qfix.engine.application import Application
from threading import Thread
from getpass import getpass

from qfix.engine.logger import setup_logger
import qfix.rabbitmq_client.consumer as rabbitmq_consumer
import qfix.rabbitmq_client.producer as rabbitmq_producer

setup_logger('FIX')
logfix = logging.getLogger('FIX')

# Auxs
def manualLogout(application):
	fix.Session.lookupSession(application.sessions[application.targetCompID]['session']).logout()

def signal_handler(sig, frame):
	manualLogout(fixMain.application)
	fixMain.initiator.stop()
	loop.close()
	sys.exit(0)

# Qfix Client Class
class main(Thread):
	def __init__(self, config_file, target, sender, username, password, account, producer, loop):
		Thread.__init__(self)
		self.config_file = config_file
		self.target = target
		self.sender = sender
		self.username = username
		self.password = password
		self.account = account

		self.settings = fix.SessionSettings(self.config_file)
		self.application = Application(self.target, self.sender, self.username, self.password, self.account, producer, loop)
		self.storefactory = fix.FileStoreFactory(self.settings)
		self.logfactory = fix.FileLogFactory(self.settings)
		self.initiator = fix.SocketInitiator(self.application, self.storefactory, self.settings, self.logfactory)

	def run(self):
		self.initiator.start()

# Main Thread
if __name__=='__main__':
	# Environment Configuration
	parser = argparse.ArgumentParser(description='FIX Client')
	parser.add_argument('file_name', type=str, help='Name of configuration file')
	args = parser.parse_args()

	target = os.environ['TARGET']
	sender = os.environ['SENDER']
	username = os.environ['USERNAME']
	password = os.environ['PASSWORD']
	account = os.environ['ACCOUNT']

	# Async
	loop = asyncio.get_event_loop()

	# RabbitMQ Producer Client
	time.sleep(15)
	producer = rabbitmq_producer.Producer(loop)
	logfix.debug("RabbitMQ Producer Client Ready")

	# Qfix Client
	fixMain = main(args.file_name, target, sender, username, password, account, producer, loop)
	fixMain.daemon = True
	fixMain.start()

	# RabbitMQ Consumer Client
	consumer = rabbitmq_consumer.Consumer(fixMain.application)
	task = loop.create_task(consumer.consume(loop))
	tasks = asyncio.gather(task)
	loop.run_until_complete(tasks)
	logfix.debug("RabbitMQ Consumer Client Ready")

	# Handler of Ctrl+C Event
	signal.signal(signal.SIGINT, signal_handler)

	time.sleep(3)

	while 1:
		time.sleep(1)

	manualLogout(fixMain.application)
	fixMain.initiator.stop()
	loop.close()