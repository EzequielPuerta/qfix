import quickfix as fix
from datetime import datetime
from abc import ABC, abstractmethod
from qfix.engine.suitable_class_finder import SuitableClassFinder
from qfix.engine.tools import printableField, fieldTag, \
	stringDatetimeAsUTC, stringDateAsUTC, stringTimeAsUTC, \
	stringDatetimeFromUTC, stringDateFromUTC, stringTimeFromUTC
from qfix.message.adapter import MessageAdapter

# Abstract field (fix.FieldBase)
class FieldDefinition(ABC):
	def __init__(self, *args, required=False, values=[], **kwargs):
		self.fixField = kwargs.pop('field')
		self.required = required
		self.values = values

		self.isEmpty = lambda a_list: len(a_list) == 0
		self.expectsValues = lambda: not(self.isEmpty(self.values))
		self.isAcceptedType = lambda value: type(value) in self.acceptedTypes()

		fixType = [validFixType for validFixType in self.validFixSuperclasses() if isinstance(self.fixField, validFixType)]
		if len(fixType) != 1:
			raise ValueError("{} is not compatible with Fix Field {}.".format(
				type(self).__name__,
				self.fieldName()))

		invalidValueTypes = [type(value) for value in self.values if not(self.isAcceptedType(value))]
		if not(self.isEmpty(invalidValueTypes)):
			raise ValueError("{}, used for Fix Field {}, is not compatible with values of types {}.".format(
				type(self).__name__,
				self.fieldName(),
				invalidValueTypes))
		super().__init__()

	def fieldName(self):
		return printableField(self.fixField)

	def fieldID(self):
		return fieldTag(self.fixField)

	def isValidValue(self, value):
		return self.isAcceptedType(value) and (not(self.expectsValues()) or (value in self.values))

	@abstractmethod
	def validFixSuperclasses(cls):
		pass

	@abstractmethod
	def acceptedTypes(self):
		pass

	@abstractmethod
	def asFIXOn(self, fixMessage, jsonMessage):
		pass

	@abstractmethod
	def asJSONOn(self, jsonMessage, fixMessage):
		pass

	def getValueOn(self, message, as_string=False):
		result = message.getValueOf(self.fixField, as_string=as_string)
		if self.required:
			if result is None:
				raise ValueError("FIX field {} expected.".format(self.fieldName()))
			else:
				if self.isValidValue(result):
					return result
				else:
					raise ValueError("'{}' is not a valid value for FIX field {}.".format(result, self.fieldName()))
		else:
			if result is None:
				return result
			else:
				if self.isValidValue(result):
					return result
				else:
					return None

# Basic field [String (fix.StringField, fix.CharField), Bool (fix.BoolField), Integer (fix.IntField, fix.CheckSumField) or Float (fix.DoubleField)]
class BasicFieldDefinition(FieldDefinition):
	def asFIXOn(self, fixMessage, jsonMessage):
		fieldValue = self.getValueOn(jsonMessage)
		if fieldValue is not None:
			self.fixField.setValue(fieldValue)
			fixMessage.setField(self.fixField)

	def asJSONOn(self, jsonMessage, fixMessage):
		fieldValue = self.getValueOn(fixMessage)
		if fieldValue is not None:
			jsonMessage[self.fieldID()] = fieldValue

class StringFieldDefinition(BasicFieldDefinition):
	def validFixSuperclasses(self):
		return [fix.StringField, fix.CharField]

	def acceptedTypes(self):
		return [str]

class IntegerFieldDefinition(BasicFieldDefinition):
	def validFixSuperclasses(self):
		return [fix.IntField, fix.CheckSumField]

	def acceptedTypes(self):
		return [int]

class FloatFieldDefinition(BasicFieldDefinition):
	def validFixSuperclasses(self):
		return [fix.DoubleField]

	def acceptedTypes(self):
		return [int, float]

class BooleanFieldDefinition(BasicFieldDefinition):
	def __init__(self, *args, **kwargs):
		try:
			kwargs.pop('values')
		except KeyError:
			pass
		super().__init__(self, *args, values=[True, False], **kwargs)

	def validFixSuperclasses(self):
		return [fix.BoolField]

	def acceptedTypes(self):
		return [bool]

# Date and time fields with string conversion (UTCTimestamp (fix.UtcTimeStampField, fix.UtcDateField, fix.UtcTimeOnlyField) or LocalMktDate (fix.StringField))
class DateTimeFieldDefinition(FieldDefinition):
	def __init__(self, *args, **kwargs):
		currentField = kwargs['field']
		self.conversionStrategy = AbstractDateTimeFieldStrategy.relatedTo(currentField)
		super().__init__(self, *args, **kwargs)

	def validFixSuperclasses(self):
		return type(self.conversionStrategy).validFixSuperclasses()

	def acceptedTypes(self):
		return [str, datetime]

	def asFIXOn(self, fixMessage, jsonMessage):
		fieldValue = self.getValueOn(jsonMessage, as_string=True)
		if fieldValue is not None:
			self.fixField.setString(self.conversionStrategy.convertTo(fieldValue))
			fixMessage.setField(self.fixField)

	def asJSONOn(self, jsonMessage, fixMessage):
		fieldValue = self.getValueOn(fixMessage, as_string=True)
		if fieldValue is not None:
			jsonMessage[self.fieldID()] = self.conversionStrategy.convertFrom(fieldValue)

# Date and time strategies
class AbstractDateTimeFieldStrategy(ABC):
	@classmethod
	def relatedTo(cls, fixField):
		try:
			subclass = SuitableClassFinder(cls).suitableFor(fixField)
			return subclass()
		except ValueError:
			raise ValueError("No datetime strategy can handle the FIX Field {}.".format(printableField(fixField)))

	@classmethod
	def canHandle(cls, fixField):
		return type(fixField).mro()[1] in cls.validFixSuperclasses()

	@classmethod
	@abstractmethod
	def validFixSuperclasses(self):
		pass

	@abstractmethod
	def convertTo(self, fieldValue):
		pass

	@abstractmethod
	def convertFrom(self, fieldValue):
		pass

class LocalDateTimeFieldStrategy(AbstractDateTimeFieldStrategy):
	@classmethod
	def validFixSuperclasses(self):
		return [fix.StringField]

	def convertTo(self, fieldValue):
		return fieldValue

	def convertFrom(self, fieldValue):
		return fieldValue

class UTCDateTimeFieldStrategy(AbstractDateTimeFieldStrategy):
	@classmethod
	def validFixSuperclasses(self):
		return [fix.UtcTimeStampField]

	def convertTo(self, fieldValue):
		return stringDatetimeAsUTC(fieldValue)

	def convertFrom(self, fieldValue):
		return stringDatetimeFromUTC(fieldValue)

class UTCDateFieldStrategy(AbstractDateTimeFieldStrategy):
	@classmethod
	def validFixSuperclasses(self):
		return [fix.UtcDateField]

	def convertTo(self, fieldValue):
		return stringDateAsUTC(fieldValue)

	def convertFrom(self, fieldValue):
		return stringDateFromUTC(fieldValue)

class UTCTimeFieldStrategy(AbstractDateTimeFieldStrategy):
	@classmethod
	def validFixSuperclasses(self):
		return [fix.UtcTimeOnlyField]

	def convertTo(self, fieldValue):
		return stringTimeAsUTC(fieldValue)

	def convertFrom(self, fieldValue):
		return stringTimeFromUTC(fieldValue)

# Space separated fields (strings with multiple values separated by spaces, like "A B C")
class SpaceSeparatedFieldDefinition(FieldDefinition):
	def validFixSuperclasses(self):
		return [fix.StringField]

	def acceptedTypes(self):
		return [str]

	def isValidValue(self, originalValue):
		separatedValues = originalValue.split()
		wrongValuesIn = lambda receivedValues: [value for value in receivedValues if value not in self.values]
		return not(self.expectsValues()) or (self.isEmpty(wrongValuesIn(separatedValues)))

	def asFIXOn(self, fixMessage, jsonMessage):
		fieldValue = self.getValueOn(jsonMessage)
		if fieldValue is not None:
			self.fixField.setValue(fieldValue)
			fixMessage.setField(self.fixField)

	def asJSONOn(self, jsonMessage, fixMessage):
		fieldValue = self.getValueOn(fixMessage)
		if fieldValue is not None:
			jsonMessage[self.fieldID()] = fieldValue

# Group data fields
class GroupDataFieldDefinition(FieldDefinition):
	def __init__(self, *args, **kwargs):
		groupField = kwargs.pop('group_field')
		self.groupedFields = kwargs.pop('grouped_fields')
		if len(self.groupedFields) == 0:
			raise ValueError("Grouped fields amount must be positive.")
		groupedRequired = [grouped.required for grouped in self.groupedFields if grouped.required]
		if groupField.required:
			if len(groupedRequired) == 0:
				raise ValueError("Group Field is set required, but all grouped fields are optional.")
		else:
			if len(groupedRequired) != 0:
				raise ValueError("Some grouped fields are required, but Group Field is set as optional.")
		super().__init__(self, *args, field=groupField.fixField, required=groupField.required, values=groupField.values, **kwargs)

	def validFixSuperclasses(self):
		return [fix.IntField]

	def acceptedTypes(self):
		return [int, list]

	def generateFixGroup(self):
		order = fix.IntArray(len(self.groupedFields)+1)
		index = 0
		for field in self.groupedFields:
			order[index] = int(field.fieldID())
			index = index+1
		order[index] = 0
		return fix.Group(self.fixField.getTag(), order[0], order)

	def asFIXOn(self, fixMessage, jsonMessage):
		groupList = self.getValueOn(jsonMessage)
		if groupList is not None:
			groupsAmount = len(groupList)
			if groupsAmount == 0:
				raise ValueError("FIX group data field {} with empty grouped fields.".format(self.fieldName()))
			else:
				group = self.generateFixGroup()
				for dataGroup in groupList:
					for groupedField in self.groupedFields:
						groupedField.asFIXOn(group, MessageAdapter.concerning(dataGroup))
					fixMessage.addGroup(group)

	def asJSONOn(self, jsonMessage, fixMessage):
		fieldValue = self.getValueOn(fixMessage)
		if fieldValue is not None:
			groupList = []
			fixGroup = self.generateFixGroup()
			for groupNumber in range(1, fieldValue+1):
				jsonGroup = {}
				fixMessage.getGroup(groupNumber, fixGroup)
				for groupedField in self.groupedFields:
					groupedField.asJSONOn(jsonGroup, MessageAdapter.concerning(fixGroup))
				groupList.append(jsonGroup)
			jsonMessage[self.fieldID()] = groupList