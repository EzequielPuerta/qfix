import quickfix as fix

class MDStatisticReqID(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 2452)
		else:
			fix.StringField.__init__(self, 2452, data)

class MDStatisticRptID(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 2453)
		else:
			fix.StringField.__init__(self, 2453, data)

class MDStatisticName(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 2454)
		else:
			fix.StringField.__init__(self, 2454, data)

class MDStatisticType(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2456)
		else:
			fix.IntField.__init__(self, 2456, data)

class MDStatisticScope(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2457)
		else:
			fix.IntField.__init__(self, 2457, data)

class MDStatisticRequestResult(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2473)
		else:
			fix.IntField.__init__(self, 2473, data)

class NoMDStatistics(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2474)
		else:
			fix.IntField.__init__(self, 2474, data)

class MDStatisticID(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 2475)
		else:
			fix.StringField.__init__(self, 2475, data)

class MDStatisticTime(fix.UtcTimeStampField):
	def __init__(self, data = None):
		if data == None:
			fix.UtcTimeStampField.__init__(self, 2476)
		else:
			fix.UtcTimeStampField.__init__(self, 2476, data)

class MDStatisticStatus(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2477)
		else:
			fix.IntField.__init__(self, 2477, data)

class MDStatisticValue(fix.DoubleField):
	def __init__(self, data = None):
		if data == None:
			fix.DoubleField.__init__(self, 2478)
		else:
			fix.DoubleField.__init__(self, 2478, data)

class MDStatisticValueType(fix.IntField):
	def __init__(self, data = None):
		if data == None:
			fix.IntField.__init__(self, 2479)
		else:
			fix.IntField.__init__(self, 2479, data)

class NumericOrderID(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 29500)
		else:
			fix.StringField.__init__(self, 29500, data)

class TradeFlag(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 29501)
		else:
			fix.StringField.__init__(self, 29501, data)

class PriceSetter(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 29502)
		else:
			fix.StringField.__init__(self, 29502, data)

class CashOrSpot(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 29503)
		else:
			fix.StringField.__init__(self, 29503, data)

class SettleByMarket(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 29504)
		else:
			fix.StringField.__init__(self, 29504, data)

class OrderBook(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 30001)
		else:
			fix.StringField.__init__(self, 30001, data)

class OrderSource(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 30004)
		else:
			if len(data) <= 10:
				fix.StringField.__init__(self, 30004, data)
			else:
				raise ValueError("'{}' is not a valid value for FIX field '{}'".format(data, fieldTag(self)))

class ConvertedYield(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 30005)
		else:
			fix.StringField.__init__(self, 30005, data)

class RFQID(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 30006)
		else:
			fix.StringField.__init__(self, 30006, data)

class ContraAccountType(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 31004)
		else:
			fix.StringField.__init__(self, 31004, data)

class ParPx(fix.StringField):
	def __init__(self, data = None):
		if data == None:
			fix.StringField.__init__(self, 32021)
		else:
			fix.StringField.__init__(self, 32021, data)