# Lambdas
def isEqualTo(an_object):
	return lambda a_field_value: a_field_value == an_object

def isNotEqualTo(an_object):
	return lambda a_field_value: a_field_value != an_object

def isEqualToAny(a_list):
	return lambda a_field_value: a_field_value in a_list

def isNotEqualToAll(a_list):
	return lambda a_field_value: not(a_field_value in a_list)

def isNone():
	return lambda a_field_value: a_field_value is None

def isNotNone():
	return lambda a_field_value: a_field_value is not None

# Conditions
class FieldCondition():
	def __init__(self, field):
		self.expectedField = field
		self.fullCondition = lambda message, index: True
		self.partialConditions = []
		self.lastPartialCondition = -1
		super().__init__()

	def fixField(self):
		return self.expectedField.fixField

	def _and(self, conditionField, conditionLambda):
		self.lastPartialCondition = self.lastPartialCondition+1
		self.partialConditions.append(self.fullCondition)
		self.fullCondition = lambda message, index: (self.partialConditions[index](message, index-1)) and (conditionLambda(message.getValueOf(conditionField)))
		return self

	def _or(self, conditionField, conditionLambda):
		self.lastPartialCondition = self.lastPartialCondition+1
		self.partialConditions.append(self.fullCondition)
		self.fullCondition = lambda message, index: (self.partialConditions[index](message, index-1)) or (conditionLambda(message.getValueOf(conditionField)))
		return self

	def _when(self, conditionField, conditionLambda):
		self._and(conditionField, conditionLambda)
		return self

	def evaluate(self, message):
		return self.fullCondition(message, self.lastPartialCondition)