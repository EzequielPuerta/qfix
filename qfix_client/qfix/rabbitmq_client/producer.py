import asyncio
import aio_pika
from aio_pika.pool import Pool
from aio_pika.message import Message, DeliveryMode
from aio_pika.exchange import Exchange, ExchangeType
from aio_pika.robust_connection import connect_robust
import qfix.rabbitmq_client.conf as conf

class Producer():
	def __init__(self, loop):
		super().__init__()
		self.loop = loop
		self.connectionPool = Pool(self.getConnection, max_size=3, loop=self.loop)
		self.exchangePool = Pool(self.getExchange, max_size=10, loop=self.loop)

	async def getConnection(self):
		return await connect_robust(
			url = conf.url,
			host = conf.host,
			port = conf.port,
			login = conf.login,
			password = conf.password,
			virtualhost = conf.virtualhost,
			ssl = conf.ssl,
			ssl_options = conf.ssl_options,
			timeout = conf.timeout,
			loop=self.loop)

	async def getExchange(self) -> Exchange:
		async with self.connectionPool.acquire() as connection:
			channel = await connection.channel(publisher_confirms=True)
			exchange = await channel.declare_exchange("publish_exchange", ExchangeType.DIRECT, durable=True)

			readyQueue = await channel.declare_queue(conf.ready_name, durable=True)
			errorsQueue = await channel.declare_queue(conf.errors_name, durable=True)
			await readyQueue.bind(exchange, routing_key=conf.ready_name)
			await errorsQueue.bind(exchange, routing_key=conf.errors_name)

			return exchange

	async def publishAt(self, jsonMessage, routingKey):
		async with self.exchangePool.acquire() as exchange:
			await exchange.publish(
				Message(body=jsonMessage.encode(),
				delivery_mode=DeliveryMode.PERSISTENT),
				routing_key=routingKey)

	async def publishAsReady(self, jsonMessage):
		await self.publishAt(jsonMessage, conf.ready_name)

	async def publishAsError(self, jsonMessage):
		await self.publishAt(jsonMessage, conf.errors_name)