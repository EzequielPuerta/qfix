import asyncio
import aio_pika
import qfix.rabbitmq_client.conf as conf

from aio_pika.tools import shield
from aio_pika.queue import Queue
from aio_pika.message import IncomingMessage
from aio_pika.exchange import ExchangeType
from aio_pika.robust_connection import connect_robust

# El aio_pika.QueueIterator me trajo problemas,
# parece ser bloqueante aún seteandole un timeout.
# https://github.com/mosquito/aio-pika/issues/122
# Me creo un QueueIterator propio para suplir este detalle
# al menos hasta encontrar una solución mejor.
class QueueIterator:
	@shield
	async def close(self):
		if not self._consumer_tag:
			return

		await self._amqp_queue.cancel(self._consumer_tag)
		self._consumer_tag = None

		def get_msg():
			try:
				return self._queue.get_nowait()
			except asyncio.QueueEmpty:
				return

		msg = get_msg()
		while msg and not self._amqp_queue.channel.closing.done():
			await msg.reject(requeue=True)
			msg = get_msg()

	def __str__(self):
		return "queue[%s](...)" % self._amqp_queue.name

	def __init__(self, queue: Queue, **kwargs):
		self.loop = queue.loop
		self._amqp_queue = queue
		self._queue = asyncio.Queue()
		self._consumer_tag = None
		self._consume_kwargs = kwargs

	async def on_message(self, message: IncomingMessage):
		await self._queue.put(message)

	async def consume(self):
		self._consumer_tag = await self._amqp_queue.consume(
			self.on_message, **self._consume_kwargs)

	def __aiter__(self):
		return self

	@shield
	async def __aenter__(self):
		if self._consumer_tag is None:
			await self.consume()
		return self

	async def __aexit__(self, exc_type, exc_val, exc_tb):
		await self.close()

	async def __anext__(self) -> IncomingMessage:
		if not self._consumer_tag:
			await self.consume()
		try:
			return await asyncio.wait_for(self._queue.get(), timeout=self._consume_kwargs.get('timeout'))
		except asyncio.CancelledError:
			await self.close()
			raise

class Consumer():
	def __init__(self, fixApplication):
		super().__init__()
		self.fixApplication = fixApplication

	async def consume(self, loop):
		connection = await connect_robust(
			url = conf.url,
			host = conf.host,
			port = conf.port,
			login = conf.login,
			password = conf.password,
			virtualhost = conf.virtualhost,
			ssl = conf.ssl,
			ssl_options = conf.ssl_options,
			timeout = conf.timeout,
			loop=loop)

		async with connection:
			channel = await connection.channel()
			exchange = await channel.declare_exchange("consumer_exchange", ExchangeType.DIRECT, durable=True)
			pendingQueue = await channel.declare_queue(conf.pending_name, durable=True)
			await pendingQueue.bind(exchange, routing_key=conf.pending_name)

			while True:
				try:
					async with QueueIterator(pendingQueue, timeout=1) as queue_iterator:
						async for message in queue_iterator:
							async with message.process():
								jsonMessageString = message.body.decode("utf-8")
								self.fixApplication.consume(jsonMessageString)
				except asyncio.TimeoutError:
					await asyncio.sleep(10)