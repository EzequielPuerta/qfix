import json
import quickfix as fix
from qfix.engine.tools import fieldTag
from qfix.message.adapter import MessageAdapter

def buildExceptionOnJSONParsing(reason, rawMessage):
	jsonReason = {}
	jsonReason['Text'] = reason
	logfix.critical("Consuming      >> | %s | %s" % (reason, rawMessage))
	return ExternalErrorMessage(code=5, type="ExceptionOnJSONParsing", reason=jsonReason, message=rawMessage).build()

def buildExceptionOnRequestMessageHandler(reason, jsonString):
	jsonReason = {}
	jsonReason['Text'] = reason
	logfix.error("Consuming      >> | %s | %s" % (reason, jsonString))
	return ExternalErrorMessage(code=1, type="ExceptionOnRequestMessageHandler", reason=jsonReason, message=jsonMessage).build()

def buildExceptionOnResponseMessageHandler(reason, fixMessage):
	jsonReason = {}
	jsonReason['Text'] = reason
	logfix.error("Publishing     << | %s | %s" % (str(Argument), printableMessage(fixMessage)))
	return ExternalErrorMessage(code=2, type="ExceptionOnResponseMessageHandler", reason=jsonReason, message=fixMessage).build()

def considerAsError(jsonMessage):
	return jsonMessage[fieldTag(fix.MsgType())] in [fix.MsgType_BusinessMessageReject, fix.MsgType_Reject]

def buildExceptionFrom(message):
	adaptedMessage = MessageAdapter.concerning(message)
	if adaptedMessage.getValueOf(fix.MsgType()) == fix.MsgType_BusinessMessageReject:
		errorCode = 3
		errorType = "MessageRejectedByMarket"
		jsonReason = reasonForMessageRejectedByMarket(adaptedMessage)
	else:
		errorCode = 4
		errorType = "MessageRejectedBySessionViolation"
		jsonReason = reasonForMessageRejectedBySessionViolation(adaptedMessage)
	logfix.critical("Publishing     << | %s" % message)
	return ExternalErrorMessage(code=errorCode, type=errorType, reason=jsonReason, message=message).build()

def reasonForMessageRejectedByMarket(adaptedMessage):
	jsonReason = {}
	jsonReason['RefSeqNum'] = adaptedMessage.getValueOf(fix.RefSeqNum())
	jsonReason['RefMsgType'] = adaptedMessage.getValueOf(fix.RefMsgType())
	jsonReason['BusinessRejectRefID'] = adaptedMessage.getValueOf(fix.BusinessRejectRefID())
	jsonReason['RefTagID'] = adaptedMessage.getValueOf(fix.RefTagID())
	jsonReason['BusinessRejectReason'] = adaptedMessage.getValueOf(fix.BusinessRejectReason())
	jsonReason['Text'] = adaptedMessage.getValueOf(fix.Text())
	return jsonReason

def reasonForMessageRejectedBySessionViolation(adaptedMessage):
	jsonReason = {}
	jsonReason['RefSeqNum'] = adaptedMessage.getValueOf(fix.RefSeqNum())
	jsonReason['RefMsgType'] = adaptedMessage.getValueOf(fix.RefMsgType())
	jsonReason['SessionRejectReason'] = adaptedMessage.getValueOf(fix.SessionRejectReason())
	jsonReason['RefTagID'] = adaptedMessage.getValueOf(fix.RefTagID())
	jsonReason['Text'] = adaptedMessage.getValueOf(fix.Text())
	return jsonReason

class ExternalErrorMessage():
	def __init__(self, *args, **kwargs):
		self.code = kwargs.pop('code')
		self.type = kwargs.pop('type')
		self.reason = kwargs.pop('reason')
		self.message = kwargs.pop('message')
		super().__init__()

	def build(self):
		jsonMessage = {}
		jsonMessage['error_code'] = self.code
		jsonMessage['error_type'] = self.type
		jsonMessage['error_reason'] = self.reason
		jsonMessage['error_message'] = self.message
		return json.dumps(jsonMessage)
