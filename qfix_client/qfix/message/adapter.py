import quickfix as fix
from abc import ABC, abstractmethod
from qfix.engine.tools import fieldTag
from qfix.engine.suitable_class_finder import SuitableClassFinder
from qfix.message.generic import hasField, getValue, getString

class MessageAdapter(ABC):
	def __init__(self, *args, **kwargs):
		self.adaptedMessage = kwargs.pop('message')
		super().__init__(*args, **kwargs)

	@classmethod
	def concerning(cls, message):
		try:
			subclass = SuitableClassFinder(cls).suitableFor(message)
			return subclass(message=message)
		except ValueError:
			raise ValueError("Message adapter not found type '{}'.".format(type(message)))

	@classmethod
	@abstractmethod
	def canHandle(cls, message):
		pass

	@abstractmethod
	def getValueOf(self, field, as_string=False):
		pass

	#@abstractmethod
	#def getGroup(groupNumber, fixGroup):
	#	pass

class FIXMessageAdapter(MessageAdapter):
	@classmethod
	def canHandle(cls, message):
		return (isinstance(message, fix.Message)) or (isinstance(message, fix.Group))

	def getValueOf(self, field, as_string=False):
		if as_string:
			return getString(self.adaptedMessage, field)
		else:
			return getValue(self.adaptedMessage, field)

	def getGroup(self, groupNumber, fixGroup):
		return self.adaptedMessage.getGroup(groupNumber, fixGroup)

class JSONMessageAdapter(MessageAdapter):
	@classmethod
	def canHandle(cls, message):
		return isinstance(message, dict)

	def getValueOf(self, field, as_string=False):
		if (fieldTag(field) in self.adaptedMessage) and as_string:
			return str(self.adaptedMessage[fieldTag(field)])
		elif (fieldTag(field) in self.adaptedMessage) and not(as_string):
			return self.adaptedMessage[fieldTag(field)]
		else:
			None