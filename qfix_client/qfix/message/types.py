import quickfix as fix
from qfix.message.generic import isMessageOfType

#---------------------------------------------------------------------
# Session and Administrative Messages
#---------------------------------------------------------------------

def isLogon(message):
	return isMessageOfType(message, fix.MsgType_Logon)

def isHeartbeat(message):
	return isMessageOfType(message, fix.MsgType_Heartbeat)

def isTestRequest(message):
	return isMessageOfType(message, fix.MsgType_TestRequest)

def isResendRequest(message):
	return isMessageOfType(message, fix.MsgType_ResendRequest)

def isReject(message):
	return isMessageOfType(message, fix.MsgType_Reject)

def isSequenceReset(message):
	return isMessageOfType(message, fix.MsgType_SequenceReset)

def isLogout(message):
	return isMessageOfType(message, fix.MsgType_Logout)

def isIndicationOfInterest(message):
	return isMessageOfType(message, fix.MsgType_IOI)

def isUserRequest(message):
	return isMessageOfType(message, fix.MsgType_UserRequest)

def isUserResponse(message):
	return isMessageOfType(message, fix.MsgType_UserResponse)

#---------------------------------------------------------------------
# Order Handling Messages
#---------------------------------------------------------------------

def isNewOrderSingle(message):
	return isMessageOfType(message, fix.MsgType_NewOrderSingle)

def isOrderCancelRequest(message):
	return isMessageOfType(message, fix.MsgType_OrderCancelRequest)

def isOrderMassCancelRequest(message):
	return isMessageOfType(message, fix.MsgType_OrderMassCancelRequest)

def isOrderMassCancelReport(message):
	return isMessageOfType(message, fix.MsgType_OrderMassCancelReport)

def isOrderCancelReplaceRequest(message):
	return isMessageOfType(message, fix.MsgType_OrderCancelReplaceRequest)

def isOrderCancelReject(message):
	return isMessageOfType(message, fix.MsgType_OrderCancelReject)

def isExecutionReport(message):
	return isMessageOfType(message, fix.MsgType_ExecutionReport)

def isOrderStatusRequest(message):
	return isMessageOfType(message, fix.MsgType_OrderStatusRequest)

def isOrderMassStatusRequest(message):
	return isMessageOfType(message, fix.MsgType_OrderMassStatusRequest)

def isTradeCaptureReportRequest(message):
	return isMessageOfType(message, fix.MsgType_TradeCaptureReportRequest)

def isTradeCaptureReport(message):
	return isMessageOfType(message, fix.MsgType_TradeCaptureReport)

def isAllocationInstruction(message):
	return isMessageOfType(message, fix.MsgType_AllocationInstruction)

def isAllocationReport(message):
	return isMessageOfType(message, fix.MsgType_AllocationReport)

#---------------------------------------------------------------------
# Market Data Messages
#---------------------------------------------------------------------

# Pre-trade Market Information
def isMarketDataRequest(message):
	return isMessageOfType(message, fix.MsgType_MarketDataRequest)

def isMarketDataSnapshotFullRefresh(message):
	return isMessageOfType(message, fix.MsgType_MarketDataSnapshotFullRefresh)

def isMarketDataIncrementalRefresh(message):
	return isMessageOfType(message, fix.MsgType_MarketDataIncrementalRefresh)

# Instrument Reference Data
def isSecurityListRequest(message):
	return isMessageOfType(message, fix.MsgType_SecurityListRequest)

def isSecurityList(message):
	return isMessageOfType(message, fix.MsgType_SecurityList)

def isTradingSessionStatusRequest(message):
	return isMessageOfType(message, fix.MsgType_TradingSessionStatusRequest)

def isTradingSessionStatus(message):
	return isMessageOfType(message, fix.MsgType_TradingSessionStatus)

def isSecurityDefinitionRequest(message):
	return isMessageOfType(message, fix.MsgType_SecurityDefinitionRequest)

def isSecurityDefinition(message):
	return isMessageOfType(message, fix.MsgType_SecurityDefinition)

def isNews(message):
	return isMessageOfType(message, fix.MsgType_News)

# Statistic Data
def isMarketDataStatisticsRequest(message):
	return isMessageOfType(message, "DO")

def isMarketDataStatisticsReport(message):
	return isMessageOfType(message, "DP")

#---------------------------------------------------------------------
# Event Messages
#---------------------------------------------------------------------

def isBusinessMessageReject(message):
	return isMessageOfType(message, fix.MsgType_BusinessMessageReject)