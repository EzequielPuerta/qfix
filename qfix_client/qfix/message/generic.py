import quickfix as fix

# BUILDERS
def buildMessageHeader(application, msgType, asReceived=False, forMillenium=True):
	msg = fix.Message()
	header = msg.getHeader()
	header.setField(fix.BeginString(fix.BeginString_FIXT11))
	header.setField(fix.MsgType(msgType))

	if forMillenium:
		header.setField(fix.DeliverToCompID('FGW'))
	else:
		header.setField(fix.DeliverToCompID('UMDF'))

	if asReceived:
		header.setField(fix.SenderCompID(application.targetCompID))
		header.setField(fix.TargetCompID(application.senderCompID))
	else:
		header.setField(fix.SenderCompID(application.senderCompID))
		header.setField(fix.TargetCompID(application.targetCompID))

	application.msg = msg
	return application.msg

# TESTING
def isMessageOfType(message, fixMessageType):
	return getHeaderValue(message, fix.MsgType()) == fixMessageType

def hasField(message, field):
	try:
		key = field
		message.getField(key)
		result = True
	except:
		result = False
	finally:
		return result

# GETTERS
def getValue(message, field):
	if hasField(message, field):
		return getBodyValue(message, field)
	else:
		try:
			return getHeaderValue(message, field)
		except:
			try:
				return getTrailerValue(message, field)
			except:
				return None

def getString(message, field):
	if hasField(message, field):
		return getBodyString(message, field)
	else:
		try:
			return getHeaderString(message, field)
		except:
			try:
				return getTrailerString(message, field)
			except:
				return None

def getBodyValue(message, field):
	key = field
	message.getField(key)
	return key.getValue()

def getBodyString(message, field):
	key = field
	message.getField(key)
	return key.getString()

def getHeaderValue(message, field):
	key = field
	message.getHeader().getField(key)
	return key.getValue()

def getHeaderString(message, field):
	key = field
	message.getHeader().getField(key)
	return key.getString()

def getTrailerValue(message, field):
	key = field
	message.getTrailer().getField(key)
	return key.getValue()

def getTrailerString(message, field):
	key = field
	message.getTrailer().getField(key)
	return key.getString()

def getValueGroup(group, field):
	key = field
	group.getField(key)
	return key.getValue()

def getStringGroup(group, field):
	key = field
	group.getField(key)
	return key.getString()