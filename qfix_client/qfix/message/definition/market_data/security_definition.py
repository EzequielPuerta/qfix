import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, datetimeField, floatField

class SecurityDefinitionDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_SecurityDefinition

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.SecurityReportID()),
			datetimeField(fix.ClearingBusinessDate()),
			stringField(fix.SecurityReqID()),
			stringField(fix.SecurityResponseID()),
			integerField(fix.SecurityResponseType()),
			stringField(fix.CorporateAction()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource()),
			stringField(fix.SecurityType()),
			stringField(fix.SecurityExchange()),
			stringField(fix.CFICode()),
			stringField(fix.Issuer()),
			floatField(fix.StrikePrice()),
			floatField(fix.ContractMultiplier()),
			datetimeField(fix.MaturityDate()),
			floatField(fix.StrikeValue()),
			datetimeField(fix.SettlDate2()),
			stringField(fix.Currency()),
			stringField(fix.Text()),
			integerField(fix.EncodedTextLen()),
			stringField(fix.EncodedText()),
			datetimeField(fix.TransactTime()),
			integerField(fix.PutOrCall()),
			integerField(fix.PriceLimitType(), values=[0, 1, 2]),
			floatField(fix.LowLimitPrice()),
			floatField(fix.HighLimitPrice())]