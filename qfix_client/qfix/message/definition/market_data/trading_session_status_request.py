import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, datetimeField

class TradingSessionStatusRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_TradingSessionStatusRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.TradSesReqID()),
			stringField(fix.MarketID()),
			stringField(fix.MarketSegmentID()),
			stringField(fix.TradingSessionID()),
			stringField(fix.TradingSessionSubID()),
			stringField(fix.TradSesMethod()),
			stringField(fix.TradSesMode()),
			stringField(fix.SubscriptionRequestType()),
			stringField(fix.SecurityExchange()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityType()),
			stringField(fix.Currency()),
			stringField(fix.SettlType()),
			datetimeField(fix.SettlDate())]