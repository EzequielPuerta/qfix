import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, booleanField, floatField, datetimeField

class SecurityListDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_SecurityList

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.SecurityReqId()),
			stringField(fix.SecurityResponseId()),
			integerField(fix.TotNoRelatedSym()),
			integerField(fix.SecurityRequestResult()),
			booleanField(fix.LastFragment()),
			groupField(
				integerField(fix.NoRelatedSym()),
					[stringField(fix.Symbol()),
					stringField(fix.SecurityID()),
					stringField(fix.SecurityIDSource()),
					stringField(fix.Text()),
					stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T", "XLINKD", "STAT"]),
					stringField(fix.SecuritySubType(), values=["M", "G", "B", "L", "P", "E", "PQ", "VQ"]),
					stringField(fix.SecurityExchange()),
					integerField(fix.Product(), values=[7, 12]),
					stringField(fix.Issuer()),
					stringField(fix.SecurityDesc()),
					stringField(fix.CFICode()),
					stringField(fix.Currency()),
					stringField(fix.SecurityStatus()),
					integerField(fix.PutOrCall(), values=[0, 1]),
					floatField(fix.StrikePrice()),
					datetimeField(fix.IssueDate()),
					integerField(fix.RoundLot()),
					groupField(
						integerField(fix.NoUnderLyings()),
							[stringField(fix.UnderlyingSymbol()),
							stringField(fix.UnderlyingSecurityID())]),
					groupField(
						integerField(fix.NoSecurityAltID()),
							[integerField(fix.SecurityAltID()),
							stringField(fix.SecurityAltIDSource())]),
					groupField(
						integerField(fix.NoTradingSessionRules()),
							[stringField(fix.TradingSessionID()),
							groupField(
								integerField(fix.NoOrdTypeRules()),
									[stringField(fix.OrdType())]),
							groupField(
								integerField(fix.NoTimeInForceRules()),
									[stringField(fix.TimeInForce())])])])]