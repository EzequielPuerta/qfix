import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField

class SecurityListRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_SecurityListRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.SecurityReqId()),
			integerField(fix.SecurityListRequestType(), values=[0, 1, 2, 3, 4]),
			stringField(fix.SecurityListType(), values=["1", "2", "3", "4"]),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T", "XLINKD", "STAT"]),
			stringField(fix.SecuritySubType(), values=["M", "G", "B", "L", "P", "E", "PQ", "VQ"]),
			stringField(fix.SecurityExchange()),
			integerField(fix.Product()),
			stringField(fix.SecurityDesc()),
			stringField(fix.CFICode()),
			stringField(fix.SubscriptionRequestType(), values=["0", "1", "2"])]