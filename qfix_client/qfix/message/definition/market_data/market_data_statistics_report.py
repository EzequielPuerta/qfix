import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, groupField, integerField, floatField, datetimeField, booleanField
from qfix.field.byma.extras import  NoMDStatistics, MDStatisticID, \
	MDStatisticRptID, MDStatisticReqID, MDStatisticRequestResult, \
	MDStatisticTime, MDStatisticStatus, MDStatisticValue, MDStatisticValueType

class MarketDataStatisticsReportDefinition(MessageDefinition):
	# Handling
	messageType = 'DP'

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(MDStatisticRptID()),
			stringField(MDStatisticReqID()),
			integerField(MDStatisticRequestResult()),
			booleanField(fix.UnsolicitedIndicator()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
			datetimeField(fix.TradeDate()),
			stringField(fix.MarketID()),
			stringField(fix.MarketSegmentID()),
			stringField(fix.MarketSegmentDesc()),
			stringField(fix.SecurityListID()),
			stringField(fix.Currency()),
			groupField(
				integerField(NoMDStatistics()),
					[stringField(MDStatisticID()),
					datetimeField(MDStatisticTime()),
					integerField(MDStatisticStatus()),
					floatField(MDStatisticValue()),
					integerField(MDStatisticValueType(), values=[1, 2])]),
			datetimeField(fix.TransactTime()),
			stringField(fix.Text())]
