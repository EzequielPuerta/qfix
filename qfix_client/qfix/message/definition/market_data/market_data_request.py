import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, booleanField, groupField, datetimeField

class MarketDataRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_MarketDataRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.MDReqID()),
			stringField(fix.SubscriptionRequestType(), values=["0", "1", "2"]),
			integerField(fix.MarketDepth()),
			integerField(fix.MDUpdateType(), values=[0, 1]),
			booleanField(fix.AggregatedBook()),
			groupField(
				integerField(fix.NoMDEntryTypes()),
					[stringField(fix.MDEntryType(), values=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "Q", "e", "x", "y"])]),
			groupField(
				integerField(fix.NoRelatedSym()),
					[stringField(fix.Symbol()),
					stringField(fix.SecurityID()),
					stringField(fix.SecurityIDSource()),
					stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T", "XLINKD", "STAT"]),
					stringField(fix.SecuritySubType(), values=["M", "G", "B", "L", "P", "E"]),
					integerField(fix.Product(), values=[7, 12]),
					stringField(fix.SecurityDesc()),
					stringField(fix.SecurityExchange()),
					stringField(fix.CFICode()),
					stringField(fix.SettlType(), values=["1", "2", "3", "N/A"]),
					datetimeField(fix.SettlDate()),
					stringField(fix.Currency())])]