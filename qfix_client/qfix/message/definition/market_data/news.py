import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField, datetimeField

class NewsDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_News

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.NewsID()),
			integerField(fix.NewsCategory()),
			stringField(fix.LanguageCode()),
			datetimeField(fix.OrigTime()),
			stringField(fix.Urgency()),
			stringField(fix.Headline()),
			integerField(fix.EncodedHeadlineLen()),
			stringField(fix.EncodedHeadline()),
			stringField(fix.MarketID()),
			stringField(fix.MarketSegmentID()),
			groupField(
				integerField(fix.NoLinesOfText()),
					[stringField(fix.Text())]),
			stringField(fix.URLLink()),
			integerField(fix.RawDataLength()),
			stringField(fix.RawData())]