import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, datetimeField, integerField

class TradingSessionStatusDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_TradingSessionStatus

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource()),
			stringField(fix.SecurityExchange()),
			stringField(fix.SecurityType()),
			stringField(fix.CFICode()),
			stringField(fix.SecurityStatus()),
			stringField(fix.SettlType()),
			datetimeField(fix.SettlDate()),
			stringField(fix.TradingSessionID()),
			integerField(fix.TradSesStatus()),
			datetimeField(fix.TradSesEndTime())]