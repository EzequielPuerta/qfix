import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, groupField, integerField, datetimeField
from qfix.field.byma.extras import MDStatisticName, MDStatisticType, \
	MDStatisticScope, NoMDStatistics, MDStatisticID

class MarketDataStatisticsRequestDefinition(MessageDefinition):
	# Handling
	messageType = 'DO'

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.MDStatisticsReqID()),
			stringField(fix.SubscriptionRequestType()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole(), values=[17, 53, 83])]),
			datetimeField(fix.TradeDate()),
			stringField(fix.MarketID()),
			stringField(fix.MarketSegmentID()),
			stringField(fix.MarketSegmentDesc()),
			stringField(fix.SecurityListID()),
			groupField(
				integerField(NoMDStatistics()),
					[stringField(MDStatisticID()),
					integerField(MDStatisticType()),
					integerField(MDStatisticScope()),
					stringField(MDStatisticName())]),
			datetimeField(fix.TransactTime()),
			stringField(fix.Text())]