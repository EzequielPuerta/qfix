import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, datetimeField, groupField

class MarketDataIncrementalRefreshDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_MarketDataIncrementalRefresh

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.MDBookType(), values=[1, 2, 3]),
			stringField(fix.MDFeedType()),
			datetimeField(fix.TradeDate()),
			stringField(fix.MDReqID()),
			groupField(
				integerField(fix.NoMDEntries()),
					[stringField(fix.MDUpdateAction(), values=["0", "1", "2"]),
					stringField(fix.MDEntryType(), values=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "Q", "e", "x", "y"]),
					stringField(fix.MDEntryID()),
					stringField(fix.Symbol()),
					stringField(fix.SecurityID()),
					stringField(fix.SecurityIDSource()),
					stringField(fix.SecurityExchange()),
					stringField(fix.CFICode()),
					stringField(fix.Issuer()),
					stringField(fix.SecurityDesc()),
					integerField(fix.Product()),
					stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T", "XLINKD", "STAT"]),
					stringField(fix.SecuritySubType(), values=["M", "G", "B", "L", "P", "E", "PQ", "VQ"]),
					floatField(fix.MDEntryPx()),
					floatField(fix.MDEntrySize()),
					datetimeField(fix.MDEntryDate()),
					datetimeField(fix.MDEntryTime()),
					stringField(fix.OrderID()),
					stringField(fix.MDEntryBuyer()),
					stringField(fix.MDEntrySeller()),
					integerField(fix.NumberOfOrders()),
					integerField(fix.MDEntryPositionNo()),
					stringField(fix.Text()),
					stringField(fix.SettlType()),
					datetimeField(fix.SettlDate())])]