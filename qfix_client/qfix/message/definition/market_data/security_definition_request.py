import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, datetimeField

class SecurityDefinitionRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_SecurityDefinitionRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.SecurityReqID()),
			integerField(fix.SecurityRequestType()),
			stringField(fix.MarketID()),
			stringField(fix.MarketSegmentID()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource()),
			stringField(fix.SecurityType()),
			stringField(fix.SecurityExchange()),
			stringField(fix.CFICode()),
			stringField(fix.Issuer()),
			floatField(fix.StrikePrice()),
			floatField(fix.ContractMultiplier()),
			datetimeField(fix.MaturityDate()),
			floatField(fix.StrikeValue()),
			stringField(fix.Currency()),
			stringField(fix.ComplianceID()),
			stringField(fix.ComplianceText()),
			integerField(fix.EncodedComplianceTextLen()),
			stringField(fix.EncodedComplianceText()),
			stringField(fix.Text()),
			integerField(fix.EncodedTextLen()),
			stringField(fix.EncodedText()),
			stringField(fix.TradingSessionID()),
			stringField(fix.TradingSessionSubID()),
			integerField(fix.ExpirationCycle()),
			stringField(fix.SubscriptionRequestType())]