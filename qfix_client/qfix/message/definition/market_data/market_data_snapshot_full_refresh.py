import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, groupField, datetimeField

class MarketDataSnapshotFullRefreshDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_MarketDataSnapshotFullRefresh

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.MDBookType(), values=[1, 2, 3]),
			stringField(fix.MDFeedType()),
			datetimeField(fix.TradeDate()),
			stringField(fix.MDReqID()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource()),
			stringField(fix.SecurityExchange()),
			stringField(fix.CFICode()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T", "XLINKD", "STAT"]),
			stringField(fix.Issuer()),
			stringField(fix.SecurityDesc()),
			groupField(
				integerField(fix.NoMDEntries()),
					[stringField(fix.MDEntryType(), values=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "Q", "e", "x", "y"]),
					stringField(fix.MDEntryID()),
					floatField(fix.MDEntryPx()),
					floatField(fix.MDEntrySize()),
					datetimeField(fix.MDEntryDate()),
					datetimeField(fix.MDEntryTime()),
					stringField(fix.MDEntryBuyer()),
					stringField(fix.MDEntrySeller()),
					stringField(fix.OrderID()),
					integerField(fix.MDEntryPositionNo()),
					stringField(fix.Text()),
					integerField(fix.NumberOfOrders()),
					stringField(fix.SettlType()),
					datetimeField(fix.SettlDate())])]