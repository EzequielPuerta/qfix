import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, integerField, stringField

class BusinessMessageRejectDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_BusinessMessageReject

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.RefSeqNum()),
			stringField(fix.RefMsgType()),
			stringField(fix.BusinessRejectRefID()),
			integerField(fix.RefTagID()),
			integerField(fix.BusinessRejectReason()),
			stringField(fix.Text())]