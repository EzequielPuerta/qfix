import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField

class HeartbeatDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_Heartbeat

	# Field definitions
	def fieldDefinitions(self):
		return [stringField(fix.TestReqID())]