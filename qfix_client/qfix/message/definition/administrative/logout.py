import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField

class LogoutDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_Logout

	# Field definitions
	def fieldDefinitions(self):
		return [stringField(fix.Text())]