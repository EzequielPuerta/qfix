import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField, integerField

class UserRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_UserResponse

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.UserRequestID()),
			stringField(fix.Username()),
			integerField(fix.UserStatus(), values=[1, 2, 3, 4, 5, 6, 7, 8, 9]),
            stringField(fix.UserStatusText())]