import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField

class TestRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_TestRequest

	# Field definitions
	def fieldDefinitions(self):
		return [stringField(fix.TestReqID())]