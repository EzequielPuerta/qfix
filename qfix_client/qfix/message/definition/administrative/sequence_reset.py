import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, integerField, booleanField

class SequenceResetDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_SequenceReset

	# Field definitions
	def fieldDefinitions(self):
		return [
			booleanField(fix.GapFillFlag()),
			integerField(fix.NewSeqNo())]