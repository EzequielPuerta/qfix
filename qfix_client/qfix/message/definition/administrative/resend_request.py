import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, integerField

class ResendRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_ResendRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.BeginSeqNo()),
			integerField(fix.EndSeqNo())]