import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField, integerField
from qfix.field.conditions import FieldCondition, isEqualTo

class UserRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_UserRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.UserRequestID()),
			integerField(fix.UserRequestType(), values=[1, 2, 3, 4, 5]),
			stringField(fix.Username())]

	# Conditions
	passwordCondition = FieldCondition(stringField(fixField=fix.Password(), required=True))._when(fix.UserRequestType(), isEqualTo(3))
	newPasswordCondition = FieldCondition(stringField(fixField=fix.NewPassword(), required=True))._when(fix.UserRequestType(), isEqualTo(3))

	# Conditional field definitions
	def conditionalFieldDefinitions(self):
		return [
			{
				"defaultField"	: stringField(fix.Password(), required=False),
				"conditions"	: [self.passwordCondition]
			},
			{
				"defaultField"	: stringField(fix.NewPassword(), required=False),
				"conditions"	: [self.newPasswordCondition]
			}
		]