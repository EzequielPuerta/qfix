import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, integerField, stringField

class RejectDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_Reject

	# Field definitions
	def fieldDefinitions(self):
		return [
			integerField(fix.RefSeqNum()),
			integerField(fix.RefTagID()),
			stringField(fix.RefMsgType()),
			integerField(fix.SessionRejectReason()),
			stringField(fix.Text())]