import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField, integerField, booleanField

class LogonDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_Logon

	# Field definitions
	def fieldDefinitions(self):
		return [
		integerField(fix.EncryptMethod(), values=[0]),
		integerField(fix.HeartBtInt()),
		integerField(fix.RawDataLength()),
		stringField(fix.RawData()),
		stringField(fix.Username()),
		stringField(fix.Password()),
		booleanField(fix.ResetSeqNumFlag()),
		integerField(fix.NextExpectedMsgSeqNum()),
		booleanField(fix.TestMessageIndicator()),
		stringField(fix.DefaultApplVerID())]