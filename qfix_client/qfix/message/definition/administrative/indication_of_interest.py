import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField, integerField, groupField

class IndicationOfInterestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_IOI

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.IOIID()),
			stringField(fix.IOITransType(), values=['N', 'C', 'R']),
			stringField(fix.Side(), values=['1', '2', '7', 'B', 'C']),
			stringField(fix.IOIQty(), values=['S', 'M', 'L', 'U']),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())])]