import json
import quickfix as fix
from abc import ABC, abstractmethod
from qfix.engine.tools import fieldTag
from qfix.engine.suitable_class_finder import SuitableClassFinder
from qfix.message.generic import buildMessageHeader
from qfix.message.adapter import MessageAdapter
from qfix.field.definitions import *

# Build a fix message from its definition and its corresponding json message (or vice versa)
class MessageDefinitionBuilder():
	def __init__(self, *args, **kwargs):
		self.fixApplication = kwargs.pop('fix_app')
		self.inputMessage = MessageAdapter.concerning(kwargs.pop('message'))
		self.builder = MessageBuilder(fix_app=self.fixApplication, message=self.inputMessage)
		self.definition = MessageDefinition.relatedTo(self.fixApplication.targetCompID, self.inputMessage.getValueOf(fix.MsgType()))
		super().__init__()

	def buildJSONMessage(self):
		return self.builder.buildAsJSONUsing(self.definition)

	def buildFIXMessage(self):
		return self.builder.buildAsFIXUsing(self.definition)

# Abstract message definition, all message definitions must inherit from this
class MessageDefinition(ABC):
	targetID = ""
	messageType = ""

	@classmethod
	def relatedTo(cls, targetCompID, messageType):
		try:
			subclass = SuitableClassFinder(cls).suitableFor(targetCompID, messageType)
			return subclass()
		except ValueError:
			raise ValueError("No message definition can handle the FIX message type '{}' for FIX session against '{}'".format(messageType, targetCompID))

	@classmethod
	def canHandle(cls, targetID, messageType):
		return (cls.messageType == messageType) and (cls.targetID == targetID if cls.targetID != "" else True)

	def headerFieldDefinitions(self):
		return [
			stringField(fix.BeginString()),
			integerField(fix.BodyLength()),
			stringField(fix.MsgType()),
			stringField(fix.ApplVerID()),
			stringField(fix.SenderCompID()),
			stringField(fix.TargetCompID()),
			integerField(fix.MsgSeqNum()),
			stringField(fix.SenderSubID()),
			stringField(fix.TargetSubID()),
			booleanField(fix.PossDupFlag()),
			booleanField(fix.PossResend()),
			datetimeField(fix.SendingTime()),
			stringField(fix.OnBehalfOfCompID()),
			stringField(fix.DeliverToCompID())]

	def fieldDefinitions(self):
		return []

	def conditionalFieldDefinitions(self):
		return []

	def trailerFieldDefinitions(self):
		return [integerField(fix.CheckSum())]

# Field constructors, to specify message definitions
def stringField(fixField, required=False, values=[]):
	return StringFieldDefinition(field=fixField, required=required, values=values)

def integerField(fixField, required=False, values=[]):
	return IntegerFieldDefinition(field=fixField, required=required, values=values)

def floatField(fixField, required=False, values=[]):
	return FloatFieldDefinition(field=fixField, required=required, values=values)

def booleanField(fixField, required=False):
	return BooleanFieldDefinition(field=fixField, required=required)

def datetimeField(fixField, required=False, values=[]):
	return DateTimeFieldDefinition(field=fixField, required=required, values=values)

def spaceSeparatedField(fixField, required=False, values=[]):
	return SpaceSeparatedFieldDefinition(field=fixField, required=required, values=values)

def groupField(groupField, groupedFields):
	return GroupDataFieldDefinition(group_field=groupField, grouped_fields=groupedFields)

# Message builder
class MessageBuilder():
	def __init__(self, *args, **kwargs):
		self.fixApplication = kwargs.pop('fix_app')
		self.message = kwargs.pop('message')
		self.fields = {}
		self.messageTypeField = stringField(fix.MsgType())
		super().__init__(*args, **kwargs)

	def add(self, fieldDefinition):
		self.fields[fieldTag(fieldDefinition.fixField)] = fieldDefinition

	def addConditionalWithDefault(self, defaultFieldDefinition, exclusive_conditions):
		defaultFieldTag = fieldTag(defaultFieldDefinition.fixField)
		try:
			conditionFieldTag = fieldTag(exclusive_conditions[0].fixField())
		except IndexError:
			raise ValueError("At least one condition must be defined for FIX Field '{}'".format(defaultFieldTag))
		else:
			should_default = True
			for condition in exclusive_conditions:
				if fieldTag(condition.fixField()) != conditionFieldTag:
					raise ValueError("Inconsistent related field in condition for FIX Field '{}'".format(defaultFieldTag))
				elif condition.evaluate(self.message):
					expectedField = condition.expectedField
					self.fields[fieldTag(expectedField.fixField)] = expectedField
					should_default = False
					break
			if should_default:
				if defaultFieldTag != conditionFieldTag:
					raise ValueError("Inconsistent default field of FIX Field '{}'".format(defaultFieldTag))
				else:
					self.fields[defaultFieldTag] = defaultFieldDefinition

	def prepareFieldsOf(self, definitions):
		for fieldDefinition in definitions:
			self.add(fieldDefinition)

	def prepareConditionalFieldsOf(self, messageDefinition):
		for conditionFieldDefinition in messageDefinition.conditionalFieldDefinitions():
			self.addConditionalWithDefault(conditionFieldDefinition["defaultField"], conditionFieldDefinition["conditions"])

	def buildAsFIXUsing(self, messageDefinition):
		self.prepareFieldsOf(messageDefinition.fieldDefinitions())
		self.prepareConditionalFieldsOf(messageDefinition)
		messageType = self.messageTypeField.getValueOn(self.message)
		fixMessage = buildMessageHeader(self.fixApplication, messageType)

		for tag, definition in self.fields.items():
			definition.asFIXOn(fixMessage, self.message)
		return fixMessage

	def buildAsJSONUsing(self, messageDefinition):
		self.prepareFieldsOf(messageDefinition.headerFieldDefinitions())
		self.prepareFieldsOf(messageDefinition.fieldDefinitions())
		self.prepareConditionalFieldsOf(messageDefinition)
		self.prepareFieldsOf(messageDefinition.trailerFieldDefinitions())
		jsonMessage = {}
		self.messageTypeField.asJSONOn(jsonMessage, self.message)

		for tag, definition in self.fields.items():
			definition.asJSONOn(jsonMessage, self.message)
		return jsonMessage