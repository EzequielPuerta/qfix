import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField, floatField, datetimeField

class AllocationInstructionDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_AllocationInstruction

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.AllocID()),
			stringField(fix.AllocTransType(), values=["0", "1", "2", "9"]),
			integerField(fix.AllocType()),
			integerField(fix.AllocNoOrdersType(), values=[0, 1]),
			stringField(fix.RefAllocID()),
			integerField(fix.AllocCancReplaceReason()),
			groupField(
				integerField(fix.NoOrders()),
					[stringField(fix.ClOrdID()),
					stringField(fix.OrderID()),
					stringField(fix.SecondaryOrderID()),
					floatField(fix.OrderQty())]),
			stringField(fix.Side()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityType()),
			stringField(fix.SecurityExchange()),
			floatField(fix.Quantity()),
			stringField(fix.Currency()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource()),
					integerField(fix.PartyRole())]),
			datetimeField(fix.TradeDate()),
			datetimeField(fix.TransactTime()),
			groupField(
				integerField(fix.NoAllocs()),
					[stringField(fix.AllocAccount()),
					floatField(fix.AllocQty())])]