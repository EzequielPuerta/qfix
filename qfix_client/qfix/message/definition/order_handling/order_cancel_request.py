import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, datetimeField, groupField
from qfix.field.byma.extras import OrderBook, OrderSource

class OrderCancelRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderCancelRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ClOrdID()),
			stringField(fix.OrigClOrdID()),
			stringField(fix.OrderID()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
			stringField(fix.Symbol()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T"]),
			stringField(fix.SecurityExchange()),
			stringField(fix.Currency()),
			stringField(fix.Side(), values=["1", "2", "5"]),
			datetimeField(fix.TransactTime()),
			stringField(fix.SettlType(), values=["1", "2", "3"]),
			datetimeField(fix.SettlDate())]

class OrderCancelRequestBYMADefinition(OrderCancelRequestDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"]),
			stringField(OrderSource())]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions