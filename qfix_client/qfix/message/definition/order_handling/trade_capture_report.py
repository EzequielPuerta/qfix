import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField, datetimeField, floatField, booleanField
from qfix.field.byma.extras import OrderBook, OrderSource, PriceSetter, \
	CashOrSpot, SettleByMarket, ContraAccountType, NumericOrderID

class TradeCaptureReportDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_TradeCaptureReport

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ApplID()),
			integerField(fix.ApplSeqNum()),
			integerField(fix.ApplLastSeqNum()),
			booleanField(fix.ApplResendFlag()),
			stringField(fix.TradeRequestID()),
			booleanField(fix.LastRptRequested()),
			stringField(fix.TradeReportID()),
			stringField(fix.TradeID()),
			stringField(fix.TradeLinkID()),
			stringField(fix.FirmTradeID()),
			stringField(fix.TradeReportRefID()),
			stringField(fix.TradeHandlingInstr()),
			integerField(fix.TradeReportType()),
			stringField(fix.ExecType(), values=["0", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]),
			integerField(fix.TradeReportTransType(), values=[0, 1, 2, 3]),
			stringField(fix.MatchStatus(), values=["0", "1"]),
			stringField(fix.OrigTradeHandlingInstr(), values=["1", "3"]),
			integerField(fix.TrdType(), values=[0, 7, 16, 22, 53, 54]),
			integerField(fix.TrdSubType()),
			datetimeField(fix.TransactTime()),
			floatField(fix.LastQty()),
			integerField(fix.PriceType(), values=[1, 2, 4, 9]),
			floatField(fix.LastPx()),
			stringField(fix.Currency()),
			floatField(fix.LastParPx()),
			floatField(fix.Yield()),
			floatField(fix.GrossTradeAmt()),
			datetimeField(fix.SettlDate()),
			stringField(fix.MatchType(), values=["1", "2", "4", "7", "22"]),
			stringField(fix.Symbol()),
			groupField(
				integerField(fix.NoSecurityAltID()),
					[stringField(fix.SecurityAltID()),
					stringField(fix.SecurityAltIDSource(), values=["4"])]),
			stringField(fix.CFICode()),
			stringField(fix.SecurityType()),
			stringField(fix.SecuritySubType()),
			stringField(fix.ProductComplex()),
			datetimeField(fix.MaturityDate()),
			groupField(
				integerField(fix.NoUnderlyings()),
					[stringField(fix.UnderlyingSymbol())]),
			integerField(fix.PutOrCall(), values=[0, 1]),
			floatField(fix.StrikePrice()),
			stringField(fix.SettlMethod(), values=["0", "1"]),
			integerField(fix.ExerciseStyle(), values=[0, 1]),
			floatField(fix.ContractMultiplier()),
			stringField(fix.Issuer()),
			datetimeField(fix.IssueDate()),
			floatField(fix.CouponRate()),
			stringField(fix.MultiLegReportingType(), values=["1", "2", "3"]),
			groupField(
				integerField(fix.NoStipulations()),
					[stringField(fix.StipulationType(), values=["TERM", "RATE"]),
					stringField(fix.StipulationValue())]),
			stringField(fix.SecondaryTradeID()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource())]

class TradeCaptureReportBYMADefinition(TradeCaptureReportDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "2", "3", "4", "6", "7"]),
			groupField(
				integerField(fix.NoSides()),
					[stringField(fix.Side(), values=["1", "2", "5"]),
					stringField(fix.SideExecID()),
					groupField(
						integerField(fix.NoPartyIDs()),
							[stringField(fix.PartyID()),
							stringField(fix.PartyIDSource()),
							integerField(fix.PartyRole())]),
					stringField(fix.Account()),
					integerField(fix.AccountType(), values=[1, 3, 100]),
					stringField(fix.TradingSessionSubID(), values=["2", "4", "6", "z"]),
					floatField(fix.AccruedInterestAmt()),
					floatField(fix.NetMoney()),
					stringField(fix.OrderCategory(), values=["1", "2", "3"]),
					integerField(fix.SideLiquidityInd(), values=[1, 2, 4]),
					stringField(fix.OrderID()),
					stringField(fix.ClOrdID()),
					stringField(fix.OrderCapacity(), values=["A", "P"]),
					stringField(fix.LotType(), values=["1", "2", "3"]),
					stringField(OrderSource())]),
			stringField(PriceSetter(), values=["0", "1"]),
			stringField(CashOrSpot(), values=["1", "2"]),
			stringField(SettleByMarket(), values=["0", "1"]),
			stringField(ContraAccountType(), values=["0", "1", "2"]),
			stringField(NumericOrderID())]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions