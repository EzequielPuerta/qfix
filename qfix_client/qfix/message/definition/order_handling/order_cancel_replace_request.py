import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, datetimeField, spaceSeparatedField, groupField
from qfix.field.byma.extras import OrderBook, OrderSource, TradeFlag

class OrderCancelReplaceRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderCancelReplaceRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ClOrdID()),
			stringField(fix.OrigClOrdID()),
			stringField(fix.OrderID()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
			stringField(fix.Symbol()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T"]),
			stringField(fix.Currency()),
			stringField(fix.OrdType()),
			stringField(fix.TimeInForce(), values=["0", "1", "2", "3", "4", "6", "7", "9"]),
			datetimeField(fix.ExpireTime()),
			datetimeField(fix.ExpireDate()),
			groupField(
				integerField(fix.NoTradingSessions()),
					[stringField(fix.TradingSessionID())]),
			spaceSeparatedField(fix.ExecInst(), values=["n", "E", "F"]),
			stringField(fix.Side(), values=["1", "2", "5"]),
			floatField(fix.OrderQty()),
			floatField(fix.DisplayQty()),
			stringField(fix.DisplayMethod(), values=["4"]),
			floatField(fix.Price()),
			floatField(fix.StopPx()),
			floatField(fix.PegOffsetValue()),
			stringField(fix.ClOrdLinkID()),
			datetimeField(fix.TransactTime()),
			stringField(fix.SettlType(), values=["1", "2", "3"]),
			datetimeField(fix.SettlDate())]

class OrderCancelReplaceRequestBYMADefinition(OrderCancelReplaceRequestDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"]),
			stringField(OrderSource()),
			stringField(TradeFlag(), values=["1", "2", "3"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions