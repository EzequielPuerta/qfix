import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField
from qfix.field.byma.extras import OrderBook

class TradeCaptureReportRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_TradeCaptureReportRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.TradeRequestID()),
			integerField(fix.TradeRequestType(), values=[0, 1]),
			stringField(fix.ExecType(), values=["0", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]),
			stringField(fix.MatchType(), values=["1", "2", "4", "7", "22"]),
			integerField(fix.TrdSubType()),
			stringField(fix.Symbol()),
			groupField(
				integerField(fix.NoSecurityAltID()),
					[stringField(fix.SecurityAltID()),
					stringField(fix.SecurityAltIDSource(), values=["4"])]),
			stringField(fix.CFICode()),
			stringField(fix.SecurityType()),
			stringField(fix.SecuritySubType()),
			stringField(fix.ProductComplex()),
			groupField(
				integerField(fix.NoUnderlyings()),
					[stringField(fix.UnderlyingSymbol())]),
			stringField(fix.Issuer()),
			stringField(fix.Side()),
			stringField(fix.ClOrdID()),
			stringField(fix.OrderID()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource()),
					integerField(fix.PartyRole())])]

class TradeCaptureReportRequestBYMADefinition(TradeCaptureReportRequestDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "2", "3", "4", "6", "7"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions