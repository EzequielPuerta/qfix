import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField, floatField, datetimeField, booleanField

class AllocationReportDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_AllocationReport

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.AllocReportID()),
			stringField(fix.AllocID()),
			stringField(fix.AllocTransType(), values=["0", "1", "2", "9"]),
			stringField(fix.AllocReportRefID()),
			integerField(fix.AllocCancReplaceReason()),
			integerField(fix.AllocReportType()),
			integerField(fix.AllocStatus()),
			integerField(fix.AllocRejCode()),
			stringField(fix.RefAllocID()),
			groupField(
				integerField(fix.NoOrders()),
					[stringField(fix.ClOrdID()),
					stringField(fix.OrderID()),
					stringField(fix.SecondaryOrderID()),
					floatField(fix.OrderQty())]),
			stringField(fix.Side()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityType()),
			stringField(fix.SecurityExchange()),
			floatField(fix.Quantity()),
			floatField(fix.AvgPx()),
			stringField(fix.Currency()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource()),
					integerField(fix.PartyRole())]),
			datetimeField(fix.TradeDate()),
			datetimeField(fix.TransactTime()),
			stringField(fix.Text()),
			integerField(fix.TotNoAllocs()),
			booleanField(fix.LastFragment()),
			groupField(
				integerField(fix.NoAllocs()),
					[stringField(fix.AllocAccount()),
					floatField(fix.AllocQty())])]