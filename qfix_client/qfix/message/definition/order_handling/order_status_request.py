import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField

class OrderStatusRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderStatusRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ClOrdID()),
			stringField(fix.Side()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource()),
					integerField(fix.PartyRole())])]