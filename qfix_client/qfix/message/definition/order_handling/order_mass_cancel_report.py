import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, stringField, integerField
from qfix.field.byma.extras import OrderBook

class OrderMassCancelReportDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderMassCancelReport

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ApplID()),
			stringField(fix.MassActionReportID()),
			stringField(fix.ClOrdID()),
			stringField(fix.MassCancelRequestType()),
			stringField(fix.MassCancelResponse()),
			integerField(fix.MassCancelRejectReason())]

class OrderMassCancelReportBYMADefinition(OrderMassCancelReportDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions