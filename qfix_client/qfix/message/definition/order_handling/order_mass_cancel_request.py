import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, datetimeField, groupField
from qfix.field.byma.extras import OrderBook

class OrderMassCancelRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderMassCancelRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ClOrdID()),
			stringField(fix.MassCancelRequestType()),
			groupField(
				integerField(fix.NoTargetPartyIDs()),
					[stringField(fix.TargetPartyID()),
					stringField(fix.TargetPartyIDSource(), values=['D']),
					integerField(fix.TargetPartyRole())]),
			stringField(fix.Symbol()),
			stringField(fix.UnderlyingSymbol()),
			stringField(fix.MarketSegmentID()),
			datetimeField(fix.TransactTime())]

class OrderMassCancelRequestBYMADefinition(OrderMassCancelRequestDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions