import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, booleanField, datetimeField, spaceSeparatedField, groupField
from qfix.field.byma.extras import OrderBook, OrderSource, TradeFlag

class NewOrderSingleDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_NewOrderSingle

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ClOrdID()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
			stringField(fix.Account()),
			integerField(fix.AccountType(), values=[1, 3, 100]),
			stringField(fix.Symbol()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T"]),
			stringField(fix.Currency()),
			stringField(fix.OrdType(), values=["1", "2", "3", "4", "K", "J"]),
			stringField(fix.TimeInForce(), values=["0", "1", "2", "3", "4", "6", "7", "9"]),
			datetimeField(fix.ExpireTime()),
			datetimeField(fix.ExpireDate()),
			groupField(
				integerField(fix.NoTradingSessions()),
					[stringField(fix.TradingSessionID())]),
			spaceSeparatedField(fix.ExecInst(), values=["n", "E", "F"]),
			stringField(fix.Side(), values=["1", "2", "5"]),
			floatField(fix.OrderQty()),
			floatField(fix.DisplayQty()),
			stringField(fix.DisplayMethod(), values=["4"]),
			floatField(fix.MinQty()),
			floatField(fix.Price()),
			floatField(fix.StopPx()),
			floatField(fix.PegOffsetValue()),
			booleanField(fix.PreTradeAnonymity()),
			stringField(fix.OrderCapacity(), values=["A", "P"]),
			stringField(fix.ClOrdLinkID()),
			stringField(fix.ClOrdLinkID()),
			datetimeField(fix.TransactTime()),
			stringField(fix.SettlType(), values=["1", "2", "3"]),
			datetimeField(fix.SettlDate())]

class NewOrderSingleBYMADefinition(NewOrderSingleDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"]),
			stringField(OrderSource()),
			stringField(TradeFlag(), values=["1", "2", "3"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions