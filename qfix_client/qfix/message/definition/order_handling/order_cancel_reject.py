import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
    stringField, integerField, datetimeField, groupField
from qfix.field.byma.extras import OrderBook

class OrderCancelRejectDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderCancelReject

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ApplID()),
			stringField(fix.ClOrdID()),
            stringField(fix.OrigClOrdID()),
			stringField(fix.OrderID()),
            groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
            stringField(fix.OrdStatus(), values=["0", "1", "2", "4", "8", "9", "A", "C", "E"]),
            stringField(fix.CxlRejResponseTo(), values=["1", "2"]),
            integerField(fix.CxlRejReason()),
            stringField(fix.Text()),
            datetimeField(fix.TransactTime())]

class OrderCancelRejectBYMADefinition(OrderCancelRejectDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions