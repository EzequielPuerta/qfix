import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, groupField

class OrderMassStatusRequestDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_OrderMassStatusRequest

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.MassStatusReqID()),
			integerField(fix.MassStatusReqType()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource()),
					integerField(fix.PartyRole())]),
			stringField(fix.Symbol()),
			stringField(fix.MarketSegmentID())]