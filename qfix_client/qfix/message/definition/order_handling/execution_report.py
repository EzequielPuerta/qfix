import quickfix as fix
from qfix.message.definition.definition import MessageDefinition, \
	stringField, integerField, floatField, booleanField, datetimeField, groupField, spaceSeparatedField
from qfix.field.byma.extras import ParPx, ConvertedYield, OrderBook, \
	OrderSource, PriceSetter, TradeFlag, NumericOrderID, RFQID

class ExecutionReportDefinition(MessageDefinition):
	# Handling
	messageType = fix.MsgType_ExecutionReport

	# Field definitions
	def fieldDefinitions(self):
		return [
			stringField(fix.ApplID()),
			stringField(fix.ExecID()),
			stringField(fix.ClOrdID()),
			stringField(fix.MDEntryID()),
			stringField(fix.OrigClOrdID()),
			stringField(fix.OrderID()),
			stringField(fix.MultiLegReportingType(), values=["1", "2", "3"]),
			stringField(fix.SecondaryOrderID()),
			stringField(fix.ExecType(), values=["0", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]),
			stringField(fix.TrdMatchID()),
			stringField(fix.ExecRefID()),
			integerField(fix.ExecRestatementReason(), values=[0, 1, 3, 8, 100]),
			stringField(fix.OrdStatus(), values=["0", "1", "2", "4", "8", "9", "A", "C", "E"]),
			booleanField(fix.WorkingIndicator()),
			integerField(fix.OrdRejReason()),
			stringField(fix.Text()),
			floatField(fix.LastQty()),
			floatField(fix.LastPx()),
			floatField(fix.LastParPx()),
			floatField(fix.Yield()),
			floatField(fix.AccruedInterestAmt()),
			floatField(fix.LeavesQty()),
			floatField(fix.CumQty()),
			floatField(fix.AvgPx()),
			stringField(fix.Symbol()),
			stringField(fix.SecurityType(), values=["CS", "GO", "OPT", "CORP", "CD", "QS", "TERM", "STN", "Plazo", "T"]),
			stringField(fix.Currency()),
			groupField(
				integerField(fix.NoPartyIDs()),
					[stringField(fix.PartyID()),
					stringField(fix.PartyIDSource(), values=['D']),
					integerField(fix.PartyRole())]),
			stringField(fix.Account()),
			integerField(fix.AccountType(), values=[1, 3, 100]),
			stringField(fix.OrdType()),
			stringField(fix.TimeInForce()),
			datetimeField(fix.ExpireTime()),
			datetimeField(fix.ExpireDate()),
			stringField(fix.TradingSessionID()),
			spaceSeparatedField(fix.ExecInst()),
			stringField(fix.Side()),
			floatField(fix.OrderQty()),
			floatField(fix.DisplayQty()),
			stringField(fix.DisplayMethod()),
			floatField(fix.MinQty()),
			floatField(fix.Price()),
			floatField(fix.StopPx()),
			floatField(fix.PegOffsetValue()),
			booleanField(fix.PreTradeAnonymity()),
			stringField(fix.OrderCapacity(), values=["A", "P"]),
			stringField(fix.ClOrdLinkID()),
			datetimeField(fix.TransactTime()),
			stringField(fix.SettlType(), values=["1", "2", "3"]),
			datetimeField(fix.SettlDate()),
			stringField(fix.SecondaryTradeID()),
			stringField(fix.SecurityID()),
			stringField(fix.SecurityIDSource())]

class ExecutionReportBYMADefinition(ExecutionReportDefinition):
	# Handling
	targetID = "STUN"

	# Field definitions
	def fieldDefinitions(self):
		definitions = super().fieldDefinitions()
		new_definitions = [
			stringField(ParPx()),
			stringField(ConvertedYield()),
			stringField(OrderBook(), values=["1", "3", "4", "5", "6", "7", "9"]),
			stringField(OrderSource()),
			stringField(PriceSetter(), values=["0", "1"]),
			stringField(NumericOrderID()),
			stringField(RFQID()),
			stringField(TradeFlag(), values=["1", "2", "3"])]
		for new_definition in new_definitions:
			definitions.append(new_definition)
		return definitions