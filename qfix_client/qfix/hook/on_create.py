import logging

def handlingWith(application, session):
	# onCreate is called when quickfix creates a new session.
	# A session comes into and remains in existence for the life of the application.
	# Sessions exist whether or not a counter party is connected to it.
	# As soon as a session is created, you can begin sending messages to it.
	# If no one is logged on, the messages will be sent at the time a connection is established with the counterparty.

	logfix = logging.getLogger('FIX')
	targetCompID = session.getTargetCompID().getValue()
	try:
		application.sessions[targetCompID]	= {}
	except AttributeError:
		application.lastOrderID				= application.account + '-00000000'
		application.orderID					= 0
		application.sessions				= {}
		application.orders					= {}
		application.sessions[targetCompID]	= {}

		application.tradeReports			= {}

	application.sessions[targetCompID]['session']   = session
	application.sessions[targetCompID]['connected'] = True
	application.sessions[targetCompID]['exchID']	= 0
	application.sessions[targetCompID]['execID']	= 0

	logfix.info("Create         >> | SessionID: (%s)" %application.sessions[targetCompID]['session'])