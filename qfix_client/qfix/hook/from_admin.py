import logging
import quickfix as fix

from qfix.message.types import *
from qfix.message.generic import hasField
from qfix.engine.session_level_messages import  *
from qfix.engine.tools import printableMessage

def handlingWith(application, message, session):
	# fromAdmin notifies you when an administrative message is sent from a counterparty to your FIX engine.
	# This can be usefull for doing extra validation on logon messages like validating passwords.
	# Throwing a RejectLogon exception will disconnect the counterparty.

	logfix = logging.getLogger('FIX')
	printable_message = printableMessage(message)

	# Administrative Messages
	if isLogon(message):
		logfix.info("Administrative << | Logon: (%s)" % printable_message)

	elif isLogout(message):
		logfix.info("Administrative << | Logout: (%s)" % printable_message)

	elif isHeartbeat(message):
		if hasField(message, fix.TestReqID()):
			application.publish(message)
			logfix.info("Administrative << | Heartbeat in response to TestRequest: (%s)" % printable_message)
		else:
			logfix.debug("Administrative << | Heartbeat: (%s)" % printable_message)

	elif isTestRequest(message):
		fix.Session.sendToTarget(testRequestHeartbeat(application, message))
		logfix.info("Administrative << | Test Request: (%s)" % printable_message)

	elif isResendRequest(message):
		logfix.info("Administrative << | Resend Request: (%s)" % printable_message)

	elif isReject(message):
		logfix.warning("Administrative << | Session Level Reject: (%s)" % printable_message)

	elif isSecuenceReset(message):
		logfix.info("Administrative << | Secuence Reset: (%s)" % printable_message)

	# Default
	else:
		logfix.info("Administrative << | Non expected: (%s)" % printable_message)