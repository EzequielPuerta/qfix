import logging

def handlingWith(aplication, session):
	# onLogon notifies you when a valid logon has been established with a counter party.
	# This is called when a connection has been established and the FIX logon process has completed with both parties exchanging valid logon messages.

	logfix = logging.getLogger('FIX')
	targetCompID = session.getTargetCompID().getValue()
	aplication.sessions[targetCompID]['connected'] = True

	logfix.info("LogOn          !! | Client %s has logged in" % targetCompID)