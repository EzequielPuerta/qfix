import logging
from qfix.message.types import *
from qfix.engine.tools import printableMessage

def handlingWith(application, message, session):
	# fromApp receives application level request.
	# If your application is a sell-side OMS, this is where you will get your new order requests.
	# If you were a buy side, you would get your execution reports here.
	# If a FieldNotFound exception is thrown,
	# the counterparty will receive a reject indicating a conditionally required field is missing.
	# The Message class will throw this exception when trying to retrieve a missing field, so you will rarely need the throw this explicitly.
	# You can also throw an UnsupportedMessageType exception.
	# This will result in the counterparty getting a reject informing them your application cannot process those types of messages.
	# An IncorrectTagValue can also be thrown if a field contains a value you do not support.

	logfix = logging.getLogger('FIX')
	printable_message = printableMessage(message)

	if isUserResponse(message):
		logfix.info("Application    << | User Response: (%s)" % printable_message)

	elif isExecutionReport(message):
		logfix.info("Application    << | Execution Report: (%s)" % printable_message)

	elif isOrderMassCancelReport(message):
		logfix.info("Application    << | Order Mass Cancel Report: (%s)" % printable_message)

	elif isOrderCancelReject(message):
		logfix.info("Application    << | Order Cancel Reject: (%s)" % printable_message)

	elif isTradeCaptureReport(message):
		logfix.info("Application    << | Trade Capture Report: (%s)" % printable_message)

	elif isAllocationReport(message):
		logfix.info("Application    << | Allocation Report: (%s)" % printable_message)

	elif isMarketDataSnapshotFullRefresh(message):
		logfix.info("Application    << | Market Data Snapshot Full Refresh: (%s)" % printable_message)

	elif isMarketDataIncrementalRefresh(message):
		logfix.info("Application    << | Market Data Incremental Refresh: (%s)" % printable_message)

	elif isSecurityList(message):
		logfix.info("Application    << | Security List: (%s)" % printable_message)

	elif isTradingSessionStatus(message):
		logfix.info("Application    << | Trading Session Status: (%s)" % printable_message)

	elif isSecurityDefinition(message):
		logfix.info("Application    << | Security Definition: (%s)" % printable_message)

	elif isMarketDataStatisticsReport(message):
		logfix.info("Application    << | Market Data Statistics Report: (%s)" % printable_message)

	elif isNews(message):
		logfix.info("Application    << | News: (%s)" % printable_message)

	elif isIndicationOfInterest(message):
		logfix.info("Application    << | Indication of Interest: (%s)" % printable_message)

	elif isBusinessMessageReject(message):
		logfix.warning("Application    << | Business Message Reject: (%s)" % printable_message)

	# Default
	else:
		logfix.info("Application    << | Not expected: (%s)" % printable_message)

	application.publish(message)