import logging

def handlingWith(aplication, session):
	# onLogout notifies you when an FIX session is no longer online.
	# This could happen during a normal logout exchange or because of a forced termination or a loss of network connection.

	logfix = logging.getLogger('FIX')
	targetCompID = session.getTargetCompID().getValue()
	aplication.sessions[targetCompID]['connected'] = False

	logfix.info("LogOut         !! | Client %s has logged out" % targetCompID)