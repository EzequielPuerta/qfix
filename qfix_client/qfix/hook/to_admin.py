import logging

from qfix.message.types import *
from qfix.engine.tools import printableMessage
from qfix.engine.session_level_messages import *

def handlingWith(application, message, session):
	# toAdmin provides you with a peek at the administrative messages that are being sent from your FIX engine
	# to the counter party. This is normally not useful for an application however it is provided for any logging
	# you may wish to do. Notice that the FIX::Message is not const.
	# This allows you to add fields to an adminstrative message before it is sent out.

	logfix = logging.getLogger('FIX')
	printable_message = printableMessage(message)

	# Administrative Messages
	if isLogon(message):
		logon(application, message)
		logfix.info("Administrative >> | Logon: (%s)" % printable_message)

	elif isLogout(message):
		logfix.info("Administrative >> | Logout: (%s)" % printable_message)

	elif isHeartbeat(message):
		logfix.debug("Administrative >> | Heartbeat: (%s)" % printable_message)

	elif isTestRequest(message):
		logfix.info("Administrative >> | Test Request: (%s)" % printable_message)

	elif isResendRequest(message):
		logfix.info("Administrative >> | Resend Request: (%s)" % printable_message)

	elif isReject(message):
		logfix.warning("Administrative >> | Session Level Reject: (%s)" % printable_message)

	elif isSecuenceReset(message):
		logfix.info("Administrative >> | Secuence Reset: (%s)" % printable_message)

	# Default
	else:
		logfix.info("Administrative >> | Non expected: (%s)" % printable_message)