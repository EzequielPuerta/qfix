import logging

from qfix.message.types import *
from qfix.engine.tools import printableMessage

def handlingWith(application, message, session):
	# toApp is a callback for application messages that are being sent to a counterparty.
	# If you throw a DoNotSend exception in this function, the application will not send the message.
	# This is mostly useful if the application has been asked to resend a message such as an order that is no longer relevant for the current market.
	# Messages that are being resent are marked with the PossDupFlag in the header set to true;
	# If a DoNotSend exception is thrown and the flag is set to true, a sequence reset will be sent in place of the message.
	# If it is set to false, the message will simply not be sent. Notice that the FIX::Message is not const.
	# This allows you to add fields to an application message before it is sent out.

	logfix = logging.getLogger('FIX')
	printable_message = printableMessage(message)

	if isUserRequest(message):
		logfix.info("Application    >> | User Request: (%s)" % printable_message)

	elif isNewOrderSingle(message):
		logfix.info("Application    >> | New Order Single: (%s)" % printable_message)

	elif isOrderCancelRequest(message):
		logfix.info("Application    >> | Order Cancel Request: (%s)" % printable_message)

	elif isOrderMassCancelRequest(message):
		logfix.info("Application    >> | Order Mass Cancel Request: (%s)" % printable_message)

	elif isOrderCancelReplaceRequest(message):
		logfix.info("Application    >> | Order Cancel Replace Request: (%s)" % printable_message)

	elif isOrderStatusRequest(message):
		logfix.info("Application    >> | Order Status Request: (%s)" % printable_message)

	elif isOrderMassStatusRequest(message):
		logfix.info("Application    >> | Order Mass Status Request: (%s)" % printable_message)

	elif isTradeCaptureReportRequest(message):
		logfix.info("Application    >> | Trade Capture Report Request: (%s)" % printable_message)

	elif isAllocationInstruction(message):
		logfix.info("Application    >> | Allocation Instruction: (%s)" % printable_message)

	elif isMarketDataRequest(message):
		logfix.info("Application    >> | Market Data Request: (%s)" % printable_message)

	elif isSecurityListRequest(message):
		logfix.info("Application    >> | Security List Request: (%s)" % printable_message)

	elif isTradingSessionStatusRequest(message):
		logfix.info("Application    >> | Trading Session Status Request: (%s)" % printable_message)

	elif isSecurityDefinitionRequest(message):
		logfix.info("Application    >> | Security Definition Request: (%s)" % printable_message)

	elif isMarketDataStatisticsRequest(message):
		logfix.info("Application    >> | Market Data Statistics Request: (%s)" % printable_message)

	elif isBusinessMessageReject(message):
		logfix.warning("Application    >> | Business Message Reject: (%s)" % printable_message)

	# Default
	else:
		logfix.info("Application    >> | Not expected: (%s)" % printable_message)