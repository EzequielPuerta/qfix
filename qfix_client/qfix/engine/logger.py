import os
import logging
from logging.handlers import TimedRotatingFileHandler
from qfix.engine.logger_handler import TimedRotatingPathHandler, SpecificLoggingLevelFilter

def setup_logger(logger_name):
	# File names
	infoLogFileName = os.path.join('.','logs','info.log')
	errorLogFileName = os.path.join('.','logs','errors.log')
	allLogFileName = os.path.join('.','logs','all.log')

	# Formatters
	simpleFormatter = logging.Formatter(fmt=u'%(asctime)s.%(msecs)03d : %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
	fullFormatter = logging.Formatter(fmt=u'%(asctime)s.%(msecs)03d %(levelname)-8s : %(message)s | (%(filename)s:%(lineno)s)', datefmt='%d/%m/%Y %H:%M:%S')

	# Handlers
	infoFileHandler = TimedRotatingPathHandler(infoLogFileName, when="midnight", interval=1, encoding='utf-8')
	errorsFileHandler = TimedRotatingPathHandler(errorLogFileName, when="midnight", interval=1, encoding='utf-8')
	allFileHandler = TimedRotatingPathHandler(allLogFileName, when="midnight", interval=1, encoding='utf-8')
	streamHandler = logging.StreamHandler()

	infoFileHandler.suffix = "%Y-%m-%d"
	errorsFileHandler.suffix = "%Y-%m-%d"
	allFileHandler.suffix = "%Y-%m-%d"

	infoFileHandler.setFormatter(simpleFormatter)
	errorsFileHandler.setFormatter(fullFormatter)
	allFileHandler.setFormatter(fullFormatter)
	streamHandler.setFormatter(simpleFormatter)

	infoFileHandler.setLevel(logging.INFO)
	infoFileHandler.addFilter(SpecificLoggingLevelFilter(logging.INFO))
	errorsFileHandler.setLevel(logging.WARNING)
	allFileHandler.setLevel(logging.DEBUG)
	streamHandler.setLevel(logging.DEBUG)

	# Logger
	logger = logging.getLogger(logger_name)
	logger.setLevel(logging.DEBUG)
	logger.addHandler(infoFileHandler)
	logger.addHandler(errorsFileHandler)
	logger.addHandler(allFileHandler)
	logger.addHandler(streamHandler)