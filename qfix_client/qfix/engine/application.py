import quickfix as fix
import quickfix50sp2 as fix50
import logging
import asyncio

import qfix.hook.on_create	as on_create
import qfix.hook.on_logon	as on_logon
import qfix.hook.on_logout	as on_logout
import qfix.hook.to_admin	as to_admin
import qfix.hook.from_admin	as from_admin
import qfix.hook.to_app		as to_app
import qfix.hook.from_app	as from_app

from qfix.engine.tools import printableMessage
from qfix.message.errors import *
from qfix.message.definition.definition import *

logfix = logging.getLogger('FIX')

class Application(fix.Application):

	def __init__(self, target, sender, username, password, account, producer, loop):
		super().__init__()
		self.targetCompID = target
		self.senderCompID = sender
		self.username = username
		self.password = password
		self.account = account
		self.producer = producer
		self.loop = loop

	def onCreate(self, session):
		on_create.handlingWith(self, session)

	def onLogon(self, session):
		on_logon.handlingWith(self, session)

	def onLogout(self, session):
		on_logout.handlingWith(self, session)

	def toAdmin(self, message, session):
		to_admin.handlingWith(self, message, session)

	def fromAdmin(self, message, session):
		from_admin.handlingWith(self, message, session)

	def toApp(self, message, session):
		to_app.handlingWith(self, message, session)

	def fromApp(self, message, session):
		from_app.handlingWith(self, message, session)

	## Messages ##
	def consume(self, rawJSONMessage):
		try:
			jsonString = json.loads(rawJSONMessage)
			logfix.debug("Consuming      >> | %s" % jsonString)
			fixMessage = MessageDefinitionBuilder(fix_app=self, message=jsonString).buildFIXMessage()
			fix.Session.sendToTarget(fixMessage)
		except json.decoder.JSONDecodeError as Argument:
			errorString = 'JSON Decode Error: {}. Look for incorrect or missing quotes, unescaped characters, comments or commas.'.format(str(Argument))
			errorMessage = buildExceptionOnJSONParsing(errorString, rawJSONMessage)
			self.loop.create_task(self.producer.publishAsError(errorMessage))
		except ValueError as Argument:
			errorMessage = buildExceptionOnRequestMessageHandler(str(Argument), jsonString)
			self.loop.create_task(self.producer.publishAsError(errorMessage))

	def publish(self, fixMessage):
		try:
			jsonMessage = MessageDefinitionBuilder(fix_app=self, message=fixMessage).buildJSONMessage()
			if considerAsError(jsonMessage):
				errorMessage = buildExceptionFrom(jsonMessage)
				self.loop.create_task(self.producer.publishAsError(errorMessage))
			else:
				jsonString = json.dumps(jsonMessage)
				logfix.debug("Publishing     << | %s" % jsonString)
				self.loop.create_task(self.producer.publishAsReady(jsonString))
		except ValueError as Argument:
			errorMessage = buildExceptionOnResponseMessageHandler(str(Argument), printableMessage(fixMessage))
			self.loop.create_task(self.producer.publishAsError(errorMessage))