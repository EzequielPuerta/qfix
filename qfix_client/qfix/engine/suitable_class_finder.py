isConcrete = lambda subclass: len(subclass.__subclasses__()) == 0

def allConcreteSubclasses(aClass):
	allSubclasses = []
	concreteSubclasses(aClass, allSubclasses)
	return allSubclasses

def concreteSubclasses(aClass, aSubclassCollection):
	for subclass in aClass.__subclasses__():
		aSubclassCollection.append(subclass) if isConcrete(subclass) else concreteSubclasses(subclass, aSubclassCollection)

class SuitableClassFinder():
	def __init__(self, abstractClass):
		self.abstractClass = abstractClass
		super().__init__()

	def suitableFor(self, *suitableObject, defaultSubclass=None, suitableMethod='canHandle'):
		allSubclasses = allConcreteSubclasses(self.abstractClass)
		filtered_subclasses = [subclass for subclass in allSubclasses if (getattr(subclass, suitableMethod)(*suitableObject))]
		if (len(filtered_subclasses) == 0) and (defaultSubclass is not None):
			return defaultSubclass
		elif (len(filtered_subclasses) == 1):
			return filtered_subclasses[0]
		elif (len(filtered_subclasses) > 1):
			raise ValueError("Many subclasses can handle the suitable object '{}'".format(suitableObject))
		else:
			raise ValueError("No subclass can handle the suitable object '{}'".format(suitableObject))