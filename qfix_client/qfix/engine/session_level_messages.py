import quickfix as fix
from datetime import datetime
from qfix.message.generic import buildMessageHeader, getValue

def logon(application, message):
	# MsgType = 'A' | sent automatically when a new session is created,
	# initiates a FIX connection to the server engine
	message.setField(fix.Username(application.username))
	message.setField(fix.Password(application.password))

def news(application):
	# MsgType = 'B' | gives information on the server connected to
	pass

def businessMessageReject(application):
	# MsgType = 'j' | indicates a client message that does not meet business
	# requirements that cannot be rejected by any other message
	pass

def heartbeat(application, heartbeatMessage):
	# MsgType = '0' | sent automatically by an engine during times
	# of inactivity to indicate that the connection is alive
	pass

def testRequestHeartbeat(application, testRequestMessage):
	# MsgType = '0' | after receiving a TestRequest message, a special
	# Heartbeat message should be sent, containing the TestReqID
	message = buildMessageHeader(application, fix.MsgType_Heartbeat)
	testReqID = getValue(testRequestMessage, fix.TestReqID())
	message.setField(fix.TestReqID(testReqID))
	return message

def testRequest(application, testMessage=datetime.timestamp(datetime.now())):
	# MsgType = '1' | the test request message forces a heartbeat
	# from the opposing application. The test request message checks
	# sequence numbers or verifies communication line status.
	# The opposite application should respond to the Test Request with a
	# Heartbeat containing the TestReqID sent. Any string can be used as
	# the TestReqID (FIX tag 112). Some fix engine generally uses a timestamp string.
	message = buildMessageHeader(application, fix.MsgType_TestRequest)
	message.setField(fix.TestReqID(str(testMessage)))
	return message

def resendRequest(application):
	# MsgType = '2' | its used for asking of re-transmission or replay of lost
	# messages during transmission or due to incorrect sequence number.
	# It is mandatory that the receiving FIX Engine process messages in sequential
	# order, e.g. if FIX message number 11 is missed and 12-13 received, the
	# application should ignore 12 and 13 and ask for a resend of 11-13, or 11 -0
	# (0 denotes infinity). Second approach is strongly recommended to recover from
	# out of sequence conditions as it allows for faster recovery in the presence of
	# certain race conditions when both sides of FIX Engines are simultaneously
	# attempting to recover a sequence number gap.
	pass

def reject(application):
	# MsgType = '3' | Session level Reject is used by FIX engine when a fix message
	# is received but cannot be properly processed due to a session-level rule
	# violation as specified in FIX Protocol specification document.
	# Rejected messages should be logged into log file and the incoming sequence
	# number must be incremented by FIX Engine.
	# As a rule FIX messages should be forwarded to the trading application for
	# business level rejections whenever possible.
	pass

def sequenceReset(application):
	# MsgType = '4' | Sequence Reset is also called as Gap fill messages. It's used in
	# response to Resend Request (FIX tag 35=2 or MsgType=2) by sending FIX engine to
	# reset the incoming sequence number on the opposing side FIX engine.
	# Sequence Reset message (fix tag 35=4) operates in two different modes one in which
	# GapFillFlag (FIX tag 123) is 'Y' also called Sequence Reset - Gap Fill and second
	# mode on which GapFillFlag (FIX tag 123) is 'N' or not present also called
	# Sequence Reset - Reset mode. As per FIX protocol specification the "Sequence Reset
	# (fix tag 35=4)-Reset" mode should ONLY be used to recover from a disaster situation
	# which cannot be otherwise recovered by "Gap Fill" mode.
	# The Sequence Reset or GapFill message (fix tag 35=4) can be useful in the following
	# circumstances :
	# 1. While processing Resend Request (FIX tag 35=2 or MsgType=2) sending FIX Engine may
	# choose not to send a message (e.g. a stale order).
	# The Sequence Reset (fix tag 35=4) - Gap Fill can be used to fill the place of that message.
	# 2. While processing Resend Request (FIX tag 35=2 or MsgType=2) sending FIX Engine may
	# choose not to send a message if all the messages are only administrative or session level
	# messages, because there is no point on resending them in that case also Sequence Reset
	# (fix tag 35=4) - Gap Fill is used to fill the message or sequence gap.
	pass

def logout(application):
	# MsgType = '5' | the FIX Logout message initiates or confirms the
	# termination of a FIX connection. Disconnection without the exchange
	# logout messages should be interpreted as an abnormal condition.
	message = buildMessageHeader(application, fix.MsgType_Logout)
	message.setField(fix.Text('Manual disconnection'))
	return message