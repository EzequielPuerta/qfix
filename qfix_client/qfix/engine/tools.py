import random
import string
from datetime import datetime
from dateutil import tz

# FIX Message tools
startOfHeading = chr(1)

def printableMessage(message):
	return message.toString().replace(startOfHeading, "|")

def randomString(stringLength=10):
	letters = string.ascii_lowercase
	return ''.join(random.choice(letters) for i in range(stringLength))

# FIX Field tools
def printableField(fixField):
	return '{}({})'.format(type(fixField).__name__, fieldTag(fixField))

def fieldTag(fixField):
	return str(fixField.getTag())

# Time conversion
utz_zone = tz.tzutc()
local_zone = tz.tzlocal()

def timestampAsUTC(formatString, externalTimestamp=None, precision=3):
	if externalTimestamp is None:
		timestamp = datetime.datetime.utcnow()
	elif type(externalTimestamp) is float:
		timestamp = datetime.datetime.utcfromtimestamp(externalTimestamp)
	else:
		timestamp = externalTimestamp

	resultingString = timestamp.strftime(formatString)
	if precision == 3:
		resultingString += ".%03d" % (timestamp.microsecond / 1000)
	elif precision == 6:
		resultingString += ".%06d" % timestamp.microsecond
	elif precision != 0:
		raise ValueError("Precision should be one of 0, 3 or 6 digits")

	return resultingString

def stringDatetimeAsUTC(externalTimestamp, formatString="%d/%m/%Y %H:%M:%S"):
	asDatetime = datetime.strptime(externalTimestamp, formatString)
	return timestampAsUTC("%Y%m%d-%H:%M:%S", asDatetime)

def stringDateAsUTC(externalDate, formatString="%d/%m/%Y"):
	asDate = datetime.strptime(externalDate, formatString)
	return timestampAsUTC("%Y%m%d", asDate)

def stringTimeAsUTC(externalTime, formatString="%H:%M:%S"):
	asTime = datetime.strptime(externalTime, formatString)
	return timestampAsUTC("%H:%M:%S", asTime)

def stringDatetimeFromUTC(externalUTC, formatString="%Y%m%d-%H:%M:%S.%f"):
	converted = datetime.strptime(externalUTC, formatString)
	converted.replace(tzinfo=utz_zone)
	converted.astimezone(local_zone)
	return timestampAsUTC("%d/%m/%Y %H:%M:%S", converted)

def stringDateFromUTC(externalUTC, formatString="%Y%m%d"):
	converted = datetime.strptime(externalUTC, formatString)
	converted.replace(tzinfo=utz_zone)
	converted.astimezone(local_zone)
	return timestampAsUTC("%d/%m/%Y", converted)

def stringTimeFromUTC(externalUTC, formatString="%H:%M:%S"):
	converted = datetime.strptime(externalUTC, formatString)
	converted.replace(tzinfo=utz_zone)
	converted.astimezone(local_zone)
	return timestampAsUTC("%H:%M:%S", converted)