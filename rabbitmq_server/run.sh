#!/bin/bash

set -e
set -u

(
count=0;
# Execute list_users until service is up and running
until timeout 5 rabbitmqctl list_users >/dev/null 2>/dev/null || (( count++ >= 60 )); do sleep 1; done;

# Enable plugins
rabbitmq-plugins enable rabbitmq_management

# Register user
if rabbitmqctl list_users | grep $RABBIT_MQ_USER > /dev/null
then
  echo "User '$RABBIT_MQ_USER' already exist, skipping user creation"
else
  echo "Creating user '$RABBIT_MQ_USER'..."
  rabbitmqctl add_user $RABBIT_MQ_USER $RABBIT_MQ_PASS
  rabbitmqctl set_user_tags $RABBIT_MQ_USER administrator
  rabbitmqctl set_permissions -p / $RABBIT_MQ_USER ".*" ".*" ".*"
  echo "User '$RABBIT_MQ_USER' creation completed"
fi
) &

# Start server
eval rabbitmq-server -d --hostname rabbitmq --name _rabbit_mq
