# QFIX

<https://gitlab.com/EzequielPuerta/qfix>

FIX Engine sobre QUICKFIX\Python

## Instalación

* Desarrollo en Python 3.8 y Docker v19.03.12 (todos los contenedores usan Linux)
* Dependencias
  * Python
    * Python 3.8
  * Quickfix
    * pip install quickfix
  * Dateutil
    * pip install python-dateutil
  * AIO-Pika
    * pip install aio-pika
  * STunnel
    * apt-get -y install stunnel
  * RabbitMQ
    * Docker container default: rabbitmq:3.7-management

## Configuración básica

* Toda la configuración esencial de QFIX se encuentra en el archivo ".env", propio de Docker.
  * El mismo se encuentra en la raiz del proyecto.
* En la sección de usuarios y grupos, se debe especificar el ID del usuario (y su grupo) con el que se desea ejecutar el contenedor
  * USER_ID puede ser el ID del propio usuario (para evitar conflictos de permisos)
    * $ id -u
  * Idem para el GROUP_ID
    * $ id -g
* En la sección de RabbitMQ se pueden definir:
  * El nombre del contenedor (no cambiarlo, si no es necesario)
  * La imagen a utilizar (por ejemplo rabbitmq:management, para usar el manager web. Sino, rabbitmq)
  * El puerto del servicio (del lado host y dentro del contenedor)
  * En caso de utilizar la imagen con el manager, se deben especificar los puertos host y container para el servidor web.
  * El usuario y la contraseña del motor rabbitmq.
* En la sección de STunnel se pueden definir:
  * El nombre del contenedor (no cambiarlo, si no es necesario)
  * El puerto a utilizar por stunnel (del lado host y dentro del contenedor)
  * La IP donde el aplicativo aceptará conexiones (localhost)
  * La IP y el puerto donde se conectará stunnel (el mercado de destino)
  * Los nombres de los archivos relacionados a la key y el certificado SSL
    * Si se tienen dichos archivos, se deben ubicar en el directorio /stunnel y definir sus nombres en la configuración.
    * Si no se tienen dichos archivos y se desea usar QFIX solo para tests, desarrollo, etc, se podrán autogenerar cuando se levante el servicio.
      * Los nombres de los archivos autogenerados seran los que se definan en la configuración
* En la sección de QFIX se pueden definir:
  * El nombre del contenedor (no cambiarlo, si no es necesario)
  * El template de configuración Quickfix a utilizar (siempre que se utilice un Initiator, se pueden agregar nuevos en qfix_client/conf).
  * Los datos de Sender, Target, Username, Password y Account (provistos por el mercado, para test_server, se pueden inventar)
  * Los diccionarios FIX de nivel de transporte y negocio (se pueden agregar nuevos en qfix_client/conf/spec).
  * El host y el puerto del socket a conectarse (puede ser la entrada de stunnel o del test_server)
* En la sección del Test Server se pueden definir:
  * El nombre del contenedor (no cambiarlo, si no es necesario)
  * El template de configuración Quickfix a utilizar (siempre que se utilice un Acceptor, se pueden agregar nuevos en test_server/conf).
  * Los diccionarios FIX de nivel de transporte y negocio (Generalmente, se querrá usar la misma versión utilizada en QFIX. Se pueden agregar nuevos en test_server/conf/spec).
  * La IP y el puerto (del lado host y container) del socket donde estará escuchando.

## Uso

* Usar QFIX contra un FIX engine remoto:
  * Copiar el diccionario FIX recibido en el directorio de especificaciones
    * $ cp ~/Market/FIX_dicc.xml ~/qfix-engine/qfix_client/conf/spec/FIX_dicc.xml
  * Editar el archivo .env con los valores provistos por el mercado
    * $ nano .env
  * Completar el directorio de stunnel con los archivos .crt, .pem
    * Sino, se autogeneran en base a los datos provistos en el archivo .env, junto con la configuración de stunnel (archivo .conf, también autogenerado)
  * Levantar el docker-compose adecuado
    * $ docker-compose -f docker-compose.yml up
  * Al finalizar, terminar la conexión
    * $ docker-compose down

* Usar QFIX contra el test_server:
  * Usar el mismo diccionario que se está usando en el cliente QFIX, en el test_server
    * $ cp ~/qfix-engine/qfix_client/conf/spec/FIX_dicc.xml ~/qfix-engine/test_server/conf/spec/FIX_dicc.xml
  * Editar el archivo .env con los valores deseados
    * $ nano .env
  * Se puede omitir completar el directorio de STunnel con los archivos .crt, .pem
    * Se autogeneran en base a los datos provistos en el archivo .env, dentro de un nuevo archivo .conf, también autogenerado
  * Levantar el docker-compose adecuado (esto ejecutará docker-compose.override.yml, que complementa el docker-compose.yml original)
    * $ docker-compose up
  * Al finalizar, terminar la conexión
    * $ docker-compose down

* En caso de haber usado un método antes de querer usar el otro (o de realizar una modificación en el código fuente), puede ser necesario hacer un build
  * $ docker-compose -f docker-compose.yml up --build
  * $ docker-compose up --build

* Si se consume demasiado espacio en disco, no se podrá levantar el contenedor nuevamente. En ese caso:
  * $ docker system prune --all --force --volumes

## Mensajería

* QFIX utiliza queues de RabbitMQ para recibir mensajes y para comunicar las respuestas a los mismos.
  * Mensajes JSON hacia el mercado
    * Queue: pendings
    * Formato: JSON con los Tags de los campos FIX deseados (clave, siempre como string) y sus atributos (valor)
    * Valores JSON aceptados, según campo FIX
      * String: para campos FIX del tipo String, MultipleStringValue, Char, MultipleCharValue, Currency, fechas y timestamps formateados (en UTC, TZ, LocalMkt o MonthYear)
      * Int: para campos FIX del tipo Int, DayOfMonth, SeqNum
      * Float: para campos FIX del tipo Float-Double, Price, PriceOffset, Qty (quantity), Percentage
      * Bool: Para campos FIX del tipo Boolean
      * Listas de JSONs: Para campos de grupos FIX (como NoPartyIDs(453)). En lugar de indicar la cantidad de bloques de repetición, se indica directamente la lista con los respectivos grupos y sus valores. Ejemplo (para el campo de grupo NoPartyIDs(453), con 3 bloques):
        * NoPartyIDs en FIX:
          * |453=3|448=dmax199cb|447=D|452=53|448=Node1|447=D|452=58|448=CL001|447=D|452=4|
        * NoPartyIDs en JSON:
          * {"453": [ {"448":"dmax199cb" , "447":"D" , "452":53}, {"448":"Node1" , "447":"D" , "452":58}, {"448":"CL001" , "447":"D" , "452":4} ]}
  * Respuestas FIX desde el mercado
    * Queue: ready
    * Formato: JSON con los Tags de los campos FIX recibidos (clave, siempre como string) y sus atributos (valor)
  * Errores
    * Queue: errors
    * Formato: JSON con los Tags de los campos FIX deseados (clave, siempre como string) y sus atributos (valor)
    * Ejemplo (se intenta enviar un mensaje TestRequest que NO contiene campo MsgType(35), y en su lugar se define erroneamente el campo 34):
      * {"error_code": 1, "error_type": "ExceptionOnRequestMessageHandler", "error_reason": {"Text": "No message definition can handle the FIX message type ''None'' for FIX session against ''BYMA'"}, "error_message": {"34": "1", "112": "fail"}}
    * Tipos de errores:
      * A completar...

## Mensajes implementados

* Administrativos:
  * Logon (se envía automáticamente al crear la sesión con éxito)
  * Logout (se envía al recibir un error, al considerar la sesión muerta o a demanda)
  * Heartbeat (se envía con cierto intervalo de tiempo según la configuración y lo acordado entre las partes, para mantener la sesión viva)
  * Test Request (ante sospecha de sesion muerta, se puede forzar el heartbeat de la contraparte con un testRequest)
  * Reject (errores encontrados en mensajes a nivel de capa de sesión / diferente al de business, de capa administrativa)
  * Indication Of Interest (se informa que la conexión fue establecida en los mercados donde se nos permite operar)
  * Resend Request (se envía para iniciar la retransmisión de ciertos mensajes, ante alguna pérdida)
  * Sequence Reset (diferentes métodos para restablecer el número de secuencia entrante en el lado opuesto, ante la presencia de inconsistencias)
  * User Request (requerir reporte de estado de usuario, o accion (inicio de sesión, desconexión, cambio de contraseña))
  * User Response (responde al requerimiento de reporte o a la acción del usuario)

* Order Routing:  
  * New Order Single (crea una nueva orden en el mercado)
  * Execution Report (respuesta a un alta, cambio de estado de la orden o error durante NewOrderSingle)
  * Allocation Instruction
  * Allocation Report
  * Order Cancel Reject
  * Order Cancel Replace Request
  * Order Cancel Request
  * Order Mass Cancel Report
  * Order Mass Cancel Request
  * Order Mass Status Request
  * Order Status Request
  * Trade Capture Report Request
  * Trade Capture Report

* Market Data:
  * Market Data Request (solicita información de mercado, instantanea o periodica de uno o mas tickers (securities))
  * Business Message Reject (rechazo por incumplimiento de alguna regla de negocio)
  * News (mensaje general de formato libre, entre el mercado y el broker. Se difunde para comunicar cuestiones generales para todo el mercado)
  * Market Data Incremental Refresh
  * Market Data Snapshot Full Refresh
  * Market Data Statistics Report
  * Market Data Statistics Request
  * Security Definition Request
  * Security Definition
  * Security List Request
  * Security List
  * Trading Session Status Request
  * Trading Session Status

## Tests

* Para ejecutar los tests, utilizar el mecanismo "discover" de la librería unittest, sobre el directorio de tests:
  * $ python -m unittest discover
* Para poder ejecutarlos, se necesitará cumplir con las dependencias de QFIX en la máquina host.
* Ahora bien, originalmente QFIX mapea el directorio de tests dentro de su propio contenedor, para poder ejecutarlos dentro de Docker. Esto se presenta como una alternativa para no tener que instalar dichas dependencias en la máquina host:
  * Entrar al contenedor de QFIX
    * $ docker exec -it qfix /bin/bash
  * También se puede entrar como usuario root
    * docker exec -it --privileged --user root qfix /bin/bash
  * Una vez dentro del contenedor, ejecutar el mecanismo "discover" de unittest
    * $ python -m unittest discover

## Links útiles

* FIX Wiki (Estructuras de los mensajes, valores posibles, tipos, versiones del protocolo, etc)
  * https://fixwiki.org/fixwiki/FIXwiki
* FIX Parser (Permite visualizar mensajes FIX de forma más amena)
  * https://fixparser.targetcompid.com/
* JSON Formatter (Formatter y validador de JSONs)
  * https://jsonformatter.curiousconcept.com/
* Documentación (poca) de QuickFIX (Útil a la hora de entender los archivos de configuración de QuickFIX, por lo demás, deja mucho que desear)
  * http://www.quickfixengine.org/quickfix/doc/html/
* Source de QuickFIX\Python (Para entender QuickFIX, mejor ir a la fuente...)
  * https://github.com/quickfix/quickfix/tree/master/src/python
