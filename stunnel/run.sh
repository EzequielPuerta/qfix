#!/bin/bash

set -e
set -u

cd $STUNNEL_PATH
CONF_FILE=stunnel.conf
CONF_TMP_FILE=$CONF_FILE.tmp

cp $STUNNEL_PATH/$CONF_TMP_FILE $CONFIG_PATH/$CONF_FILE
eval "echo \"$(cat $CONFIG_PATH/stunnel.conf)\" > $CONFIG_PATH/stunnel.conf"

if [ ! -f $STUNNEL_PATH/$KEYFILE ] ; then
    openssl genrsa -out $STUNNEL_PATH/$KEYFILE
    echo "New OpenSSL Key generated"
fi

chmod 400 $STUNNEL_PATH/$KEYFILE

if [ ! -f $STUNNEL_PATH/$CERTFILE ] ; then
    CSRFILE="`dirname $STUNNEL_PATH/$CERTFILE`/`basename $STUNNEL_PATH/$CERTFILE .crt1`.csr"
    openssl req -new -key $STUNNEL_PATH/$KEYFILE -subj "/CN=$CERTNAME" -out $CSRFILE
    openssl x509 -req -days 365 -in $CSRFILE -out $STUNNEL_PATH/$CERTFILE -signkey $STUNNEL_PATH/$KEYFILE
    echo "New OpenSSL Certificate generated"
fi

eval stunnel4 $CONFIG_PATH/$CONF_FILE
